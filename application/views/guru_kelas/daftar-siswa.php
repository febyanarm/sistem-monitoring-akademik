<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Data Mata Pelajaran - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Mata Pelajaran</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table
                          id="table"
                          data-toggle="table"
                          data-url="<?php echo base_url(); ?>index.php/guru_kelas/daftar_siswa" 
                          data-show-refresh="true"
                          data-show-toggle="true"
                          data-show-columns="true"
                          data-search="true"
                          data-select-item-name="toolbar1"
                          data-toolbar="#toolbar"
                          data-pagination="true"
                          data-sort-name="name"
                          data-sort-order="desc" 
                          data-show-export="true">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">Nomor Induk Siswa</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="agama"  data-sortable="true">Agama</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Pendidikan Sebelum</th>
                                  <th data-field="kelainan" data-sortable="true">Kelainan</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Nama Ayah</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php foreach ($getdata_siswaAll as $row): ?>
                                <tr>

                                  <!--<td><?php //echo $row->idmerk; ?></td>-->
                                  <td>
                                  <img id="foto" src="<?php echo base_url().'assets/images/siswa/'.$row->foto ?>" style="height: 100px; width: auto;">
                                  </td>
                                  <td><?php echo $row->no_induk; ?></td>
                                  <td><?php echo $row->nama_siswa; ?></td>
                                  <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                                  <td><?php echo $row->jenis_kelamin; ?></td>
                                  <td><?php echo $row->agama; ?></td>
                                  <td><?php echo $row->pendidikan_sebelum; ?></td>
                                  <td><?php echo $row->kelainan; ?></td>
                                  <td><?php echo $row->nama_ayah; ?></td>
                                  <td>
                                  <form action="<?php echo base_url();?>index.php/Admin/update_merk" method="POST" style="display: inline !important;">
                                  <input type="hidden" value="<?php echo $row->no_induk; ?>" name="nopolisi">
                                  <button type="submit" class="btn btn-success" value="Update" name="btnsubmit" title="Ubah"><i class="fa fa-edit" aria-hidden="true"></i></button><i class="" aria-hidden="true">
                                  </form>
                                  <form action="<?php echo base_url();?>index.php/Admin/hapus_merk" method="POST" style="display: inline !important;" style="display: inline-block;">
                                  <input type="hidden" name="iderk" value="<?php echo $row->no_induk; ?>">
                                  <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  </form>

                                  </td>
                                </tr>
                              <?php endforeach ?>
                          </tbody>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
   function tampilFoto(){
    document.getElementById('tampilFoto1').setAttribute('type','hidden');
  }
</script>
         


 