<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Data Siswa - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Siswa</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/siswa/getdata_siswaAll"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-show-print="true" 
                          data-show-export="true">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="id" data-sortable="true">Nomor Induk Siswa</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="agama"  data-sortable="true">Agama</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Pendidikan Sebelum</th>
                                  <th data-field="kelainan" data-sortable="true">Kelainan</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Nama Ayah</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_siswaAll as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->no_induk; ?></td>
                              <td><b><?php echo $row->nama_siswa; ?></b></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->agama; ?></td>
                              <td><?php echo $row->pendidikan_sebelum; ?></td>
                              <td><?php echo $row->kelainan; ?></td>
                              <td><?php echo $row->nama_ayah; ?></td>
                              <td>
                              <form action="<?php echo base_url();?>index.php/Admin/hapus_merk" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-edit" aria-hidden="true"></i> Masukkan Nilai</button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
   function tampilFoto(){
    document.getElementById('tampilFoto1').setAttribute('type','hidden');
  }
</script>
         


 
 