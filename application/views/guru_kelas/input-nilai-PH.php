<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Penilaian Mingguan - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">PENILAIAN MINGGUAN</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/siswa/getdata_siswaAll"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-show-print="true" 
                          data-show-export="true">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="id" data-sortable="true">Nomor Induk Siswa</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="agama"  data-sortable="true">Kelas</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Input Nilai</th>
                              </thead>
                              <?php foreach ($getdata_siswaKelas as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->no_induk; ?></td>
                              <td><b><?php echo $row->nama_siswa; ?></b></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->tingkat_kelas;echo ' - ';  echo $row->nama_kelas; ?></td>
                              <td>
                              <input type="hidden" value="<?php echo $row->no_induk; ?>" id="<?php echo $row->no_induk; ?>" name="">
                                <button type="button" data-nis="<?php echo $row->no_induk; ?>" data-nama="<?php echo $row->nama_siswa; ?>" id="pilih" class="btn btn-default" href="#myModal" data-toggle="modal" data-target="#myModal"  onclick="hitungMinggu(getElementById('<?php echo $row->no_induk; ?>').value);"><b>Masukkan Nilai</b></button>
                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Penilaian</h4></center>
                    </div>
                    <div class="modal-body">

                    <div class="container">
                      <div class="row">
                            <form>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="form-group">
                                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="no_induk">
                                     NIS
                                      </label>
                                      <div class="col-md-5 col-sm-5 col-xs-12">
                                        <input type="text" readonly id="nis" class="form-control col-md-7 col-xs-12" ">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="no_induk">
                                     Nama
                                      </label>
                                      <div class="col-md-5 col-sm-5 col-xs-12">
                                        <input type="text" readonly id="namasiswa" class="form-control col-md-7 col-xs-12" ">
                                      </div>
                                 </div>
                                 <div id="infoPRB">
                                   
                                 </div>
                              </div>
                            </form>
                        </div>
                      </div>
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Guru_kelas/input_nilai" method="POST">
                          <?php foreach ($mapel_denganSikap as $row1): ?>
                             <?php echo $row1->nama_mapel; ?>
                          <?php endforeach ?>

                                <input type="hidden"  value="PH" class="form-control col-md-7 col-xs-12" name="kriteria">
                                <input type="hidden" id="nis1" value="" class="form-control col-md-7 col-xs-12" name="nis">
                                <input type="hidden" id="namasiswa1" value="" class="form-control col-md-7 col-xs-12" name="namasiswa">

                            <div class="form-group">
                              <label class="col-md-2 col-sm-4 col-xs-12 col-md-offset-5 col-sm-offset-6" for="no_induk" style="color: #5bc0de; font-weight: bold;">
                              PENGETAHUAN
                              </label>
                              <label class="col-md-2 col-sm-4 col-xs-12" for="no_induk" style="color: #5bc0de; font-weight: bold;">
                             KETERAMPILAN
                              </label>
                              </div>
                          <?php foreach ($getdata_kbm as $row): ?>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12 col-md-offset-2" for="no_induk">
                             <?php echo $row->nama_mapel; ?>
                              </label>
                              <div class="col-md-2 col-sm-4 col-xs-12">
                                <input type="hidden"  value="<?php echo $row->id_jadwal; ?>" class="form-control col-md-7 col-xs-12" name="id_jadwal[]">
                                <input type="text"  value="" class="form-control col-md-7 col-xs-12" name="peng[]">
                              </div>
                              <div class="col-md-2 col-sm-4 col-xs-12">
                                <input type="text"  value="" class="form-control col-md-7 col-xs-12" name="ket[]">
                              </div>
                            </div>
                          <?php endforeach ?>
                       <!-- ============================================= form ==================================-->
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                              <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript">
   $(document).on('click', '#pilih', function (e) {
              document.getElementById('nis').value =  $(this).attr('data-nis');
              document.getElementById('namasiswa').value =  $(this).attr('data-nama');
              document.getElementById('nis1').value =  $(this).attr('data-nis');
              document.getElementById('namasiswa1').value =  $(this).attr('data-nama');
                $('#myModal').modal('hide');
            });
</script>
         


 
 