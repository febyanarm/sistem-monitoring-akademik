<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Data Mata Pelajaran - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Selamat Datang Di Halaman Utama Guru Kelas Aplikasi SIMONIK</h3></center>
                      <div class="panel-body">
                          <!-- Load chart dengan menggunakan ID -->

<?php
    /* Mengambil query report*/
    foreach($report as $result){
        $bulan[] = $result->periode; //ambil bulan
    }
    /* end mengambil query*/

    /*mengambil tahun ajaan*/
    if(date('m') < 7){
            $year = date('Y');
            $first = $year-1;
            $last = date('Y');
                }
          else{
            $year = date('Y');
            $first = date('Y');
            $last = $year+1;
        }
    /*end mengambil tahun ajaan*/

    $id_user = $this->session->userdata('id_user');
    $role = $this->session->userdata('role');
     
?>
<button class="btn btn-default pull-right" onclick="printDiv();">Cetak</button>
<div id="print">
<div id="ph"></div>
                    <hr style="margin-top: 0; ">
<div id="pts"></div>
                    <hr style="margin-top: 0; ">
<div id="pas"></div>
</div>
<!-- END load chart -->
 
<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#ph').highcharts({
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '<?php echo 'Tahun Ajaran '.$first.' - '.$last; ?>',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Penilaian Mingguan',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Nilai'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [
                <?php
                  $query = $this->db->query("select DISTINCT nama_siswa from rata_periode_nonss,mata_pelajaran where no_induk_pegawai = '$id_user' and mata_pelajaran.author = '$role' and mata_pelajaran.id_mapel = rata_periode_nonss.id_mapel and kriteria = 'PH'"); 
                  $nama = $query->result();

                  foreach ($nama as $key) {
                    $nama_siswa = $key->nama_siswa;
                    $query1 = $this->db->query("select rata from rata_periode_nonss where nama_siswa = '$nama_siswa'"); 
                    $nama1 = $query1->result_array();
                    $nama2 = array();
                    unset($nama2);
                    foreach($nama1 as $key1){
                     $nama2[] = (float)$key1['rata'];
                    }
                ?>
                {
                    name: <?php echo json_encode($nama_siswa);?>,
                    data: <?php echo json_encode($nama2);?>,
                    shadow : true,
                    dataLabels: {
                        enabled: true,
                        color: '#045396',
                        align: 'center',
                        formatter: function() {
                             return Highcharts.numberFormat(this.y, 0);
                        }, // one decimal
                        y: 0, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
        },
              <?php }  ?>
        ]
    });
});


$(function () {
    $('#pts').highcharts({
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '<?php echo 'Tahun Ajaran '.$first.' - '.$last; ?>',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Penilaian Tengah Semester',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Nilai'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [
                <?php
                  $query = $this->db->query("select DISTINCT nama_siswa from rata_periode_nonss,mata_pelajaran where no_induk_pegawai = '$id_user' and mata_pelajaran.author = '$role' and mata_pelajaran.id_mapel = rata_periode_nonss.id_mapel and kriteria = 'PTS'"); 
                  $nama = $query->result();

                  foreach ($nama as $key) {
                    $nama_siswa = $key->nama_siswa;
                    $query1 = $this->db->query("select rata from rata_periode_nonss where nama_siswa = '$nama_siswa'"); 
                    $nama1 = $query1->result_array();
                    $nama2 = array();
                    unset($nama2);
                    foreach($nama1 as $key1){
                     $nama2[] = (float)$key1['rata'];
                    }
                ?>
                {
                    name: <?php echo json_encode($nama_siswa);?>,
                    data: <?php echo json_encode($nama2);?>,
                    shadow : true,
                    dataLabels: {
                        enabled: true,
                        color: '#045396',
                        align: 'center',
                        formatter: function() {
                             return Highcharts.numberFormat(this.y, 0);
                        }, // one decimal
                        y: 0, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
        },
              <?php }  ?>
        ]
    });
});


$(function () {
    $('#pas').highcharts({
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '<?php echo 'Tahun Ajaran '.$first.' - '.$last; ?>',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Penilaian Akhir Semester',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Nilai'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [
                <?php
                  $query = $this->db->query("select DISTINCT nama_siswa from rata_periode_nonss,mata_pelajaran where no_induk_pegawai = '$id_user' and mata_pelajaran.author = '$role' and mata_pelajaran.id_mapel = rata_periode_nonss.id_mapel and kriteria = 'PAS'"); 
                  $nama = $query->result();

                  foreach ($nama as $key) {
                    $nama_siswa = $key->nama_siswa;
                    $query1 = $this->db->query("select rata from rata_periode_nonss where nama_siswa = '$nama_siswa'"); 
                    $nama1 = $query1->result_array();
                    $nama2 = array();
                    unset($nama2);
                    foreach($nama1 as $key1){
                     $nama2[] = (float)$key1['rata'];
                    }
                ?>
                {
                    name: <?php echo json_encode($nama_siswa);?>,
                    data: <?php echo json_encode($nama2);?>,
                    shadow : true,
                    dataLabels: {
                        enabled: true,
                        color: '#045396',
                        align: 'center',
                        formatter: function() {
                             return Highcharts.numberFormat(this.y, 0);
                        }, // one decimal
                        y: 0, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
        },
              <?php }  ?>
        ]
    });
});
        </script>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
   function tampilFoto(){
    document.getElementById('tampilFoto1').setAttribute('type','hidden');
  }
</script>
         

