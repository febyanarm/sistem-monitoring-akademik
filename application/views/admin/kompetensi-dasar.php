<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Kompetensi Dasar - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Komepetensi Dasar</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">

               
<!-- ============================================= form ==================================-->
       <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
             <!-- garis --><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="alert alert-info" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <br>1. pastikan anda memilih mobil yang sesuai dengan kebutuhan anda
                        <br>2. pilih dan masukkan data pada form di bawah ini
                        <br>3. setelah anda memasukkan data kami akan mengirim pemberitahuan kepada para pemilik mobil
                        <br>4. harap tunggu pemberitahuan selanjutnya berupa penawaran harga dari pemilik mobil
                        <br>5. kami akan mengirim pemberitahuan melalui email dan halaman penawaran pada dashboard anda
                  </div>
                <div class="x_panel collapse-div" style="background: darkseagreen;">
                <a class="collapse-link" style="cursor: pointer;">
                  <div class="x_title">
                    <h2>Tambah Data Komepetensi Dasar <small>(klik di sini untuk menambahkan data)</small></h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><i class="fa fa-chevron-up"></i></li>
                    </ul> -->
              <!-- garis --><div class="clearfix"></div>
              </a>

                  </div>
                  <div class="x_content" style="display: none;">
                    <br />
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_KD" method="POST">
                              <input type="hidden" class="form-control" name="id_kd" value="<?php echo $getid_KD; ?>">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="id_mapel">
                            ID Mata Pelajaran
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <select class="form-control" name='id_mapel' id='id_mapel'">
                                <option value="">Pilih Mata Pelajaran</option>
                                <?php //if($list->num_rows > 0) ?>
                                <?php foreach ($getdata_mapel as $row): ?>
                                <?php echo '<option value="'.$row->id_mapel.'">'.$row->nama_mapel.'</option><br>'; ?>
                                <?php endforeach ?>
                              </select>             
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                            Tingkat Kelas
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" name='tingkat' id='tingkat'">
                                <option value="">Pilih Tingkat Kelas</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                              </select>  
                              </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Nomor KD<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" class="form-control" data-inputmask="'mask': '9.99'"" placeholder="Contoh : 1.1 , 1.11" name="no_kd">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Keterangan<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" class="form-control" name="keterangan">
                            </div>
                          </div>
                          <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                            <button class="btn btn-success" type="reset">Urungkan</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/admin/data_KD"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="id" data-sortable="true">Id Kompetensi Dasar</th>
                                  <th data-field="nama"  data-sortable="true">Mata Pelajaran</th>
                                  <th data-field="tingkat"  data-sortable="true">Tingkat Kelas</th>
                                  <th data-field="nokd"  data-sortable="true">No. KD</th>
                                  <th data-field="ket"  data-sortable="true">Ketrangan</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_allKD as $row): ?>
                            <tr>

                              <!--<td><?php echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->id_kd; ?></td>
                              <td><?php echo $row->nama_mapel; ?></td>
                              <td><?php echo $row->tingkat_kelas; ?></td>
                              <td><?php echo $row->no_kd; ?></td>
                              <td><?php echo $row->keterangan; ?></td>
                              <td>

                             <button type="button" class="edit btn btn-success" href="#myModal" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row->id_mapel; ?>" data-nama="<?php echo $row->nama_mapel; ?>" data-auth="<?php echo $row->author; ?>"><i class="fa fa-edit" aria-hidden="true"></i></button>
                           
                              <form action="<?php echo base_url();?>index.php/admin/hapus_mapel" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="hapus_mapel" value="<?php echo $row->id_mapel; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
</div>

 

<script type="text/javascript">

//            jika dipilih, nim akan masuk ke input dan modal di tutup
          $(document).on('click', '.edit', function (e) {
                document.getElementById("id_mapel_edit").value = $(this).attr('data-id');
                document.getElementById("nama_mapel").value = $(this).attr('data-nama');
                document.getElementById("author").value = $(this).attr('data-auth');
            });
            
</script>