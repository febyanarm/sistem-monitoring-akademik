<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Data Siswa - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                  <?php foreach ($judulLihatIPK as $row1): ?>
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data IPK <?php echo $row1->nama_mapel; ?> - Kelas <?php echo $row1->tingkat_kelas; ?></h3>
                  <?php endforeach ?>
                    <small><b>No. KD : <?php echo $row1->no_kd; ?> - <?php echo $row1->keterangan; ?></b></small></center>
                    <hr style="margin-top: 0; ">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/siswa/getdata_siswaAll"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">ID IPK</th>
                                  <th data-field="id" data-sortable="true">Nomor IPK</th>
                                  <th data-field="name"  data-sortable="true">Keteragan</th>
                              </tr>
                              </thead>
                              <?php foreach ($lihatIPK as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->id_ipk; ?></td>
                              <td><?php echo $row->no_ipk; ?></td>
                              <td><?php echo $row->keterangan; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
   function tampilFoto(){
    document.getElementById('tampilFoto1').setAttribute('type','hidden');
  }
</script>
         


 