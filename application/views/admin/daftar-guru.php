<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> <title>Data Mata Pelajaran - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Daftar Guru Pengajar</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body" id="panelSaya">
                          <table data-toggle="table" id="tableSaya" data-url="<?php echo base_url(); ?>index.php/admin/daftar_guru"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-show-print="true" 
                          data-show-export="true">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">NIP</th>
                                  <th data-field="name"  data-sortable="true">Nama Guru</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Mulai Mengajar</th>
                                  <th data-field="kelainan" data-sortable="true">Telepon</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Status</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_dataGuruAll as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td>
                              <img id="foto" src="<?php echo base_url().'assets/images/guru/'.$row->foto ?>" style="height: 100px; width: auto;">
                              </td>
                              <td><?php echo $row->no_induk_pegawai; ?></td>
                              <td><?php echo $row->nama_guru; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tgl_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->mulai_mengajar; ?></td>
                              <td><?php echo $row->telp; ?></td>
                              <td><?php echo $row->status; ?></td>
                              <td>
                              <form action="<?php echo base_url();?>index.php/Admin/edit_guru" method="POST" style="display: inline !important;">
                              <input type="hidden" value="<?php echo $row->no_induk_pegawai; ?>" name="no_induk_pegawai">
                              <button type="submit" class="btn btn-success" value="Update" name="btnsubmit" title="Ubah"><i class="fa fa-edit" aria-hidden="true"></i></button><i class="" aria-hidden="true">
                              </form>
                              <form action="<?php echo base_url();?>index.php/Admin/hapus_guru" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="no_induk_pegawai" value="<?php echo $row->no_induk_pegawai; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
</script>
         


 