<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Data Guru - Sistem Monitoring Akademik</title>
<style type="text/css">
  .stepContainer{
    height: 10px !important;
  }
</style>
<div class="right_col" role="main">
                    <!--<hr style="margin-top: 0; ">-->
<!--start-->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <center>
                    <h3 style="margin-bottom: 0; color:#26b99a; ">Formulir Data Guru Pengajar</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small>
                </center>
                  <div class="x_content">
                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Informasi Pribadi<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Alamat<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Data Kepegawaian<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <!--
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Step 4 description</small>
                                          </span>
                          </a>
                        </li>
                        -->
                      </ul>
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/input_guru" method="POST">
                      <div id="step-1">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                            Nomor Induk Pegawai (NIP)<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="no_induk" class="form-control col-md-7 col-xs-12" name="no_induk_pegawai" required="">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Nama Guru<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                              <input type="text" required="required" class="has-feedback-left form-control col-md-7 col-xs-12" name="nama_guru">
                              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="tempat_lahir">
                            Tempat Lahir<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="tempat_lahir" required="required" class="form-control col-md-7 col-xs-12" name="tempat_lahir">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">Tanggal Lahir<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                                  <input type="date" class="form-control" name="tgl_lahir">
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Jenis Kelamin<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                                <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="laki-laki" checked="" required />&nbsp;Laki - Laki&nbsp;&nbsp;&nbsp;
                                <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="perempuan" />&nbsp;Perempuan
                              </p>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Agama<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <select id="heard" class="form-control" required name="agama">
                                <option value="">- Pilih Agama -</option>
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="budha">Budha</option>
                                <option value="konghuchu">Konghuchu</option>
                                <option value="hindu">Hindu</option>
                              </select>
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">Foto<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                                  <input type="file" class="form-control" placeholder="" name="userfile"  name="tanggal_lahir">
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="tempat_lahir">
                            Nomor Telepon / HP<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="tempat_lahir" required="required" class="form-control col-md-7 col-xs-12" name="telp">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="tempat_lahir">
                            Riwayat Pendidikan<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="tempat_lahir" required="required" class="form-control col-md-7 col-xs-12" name="riw_pend">
                            </div>
                          </div>
                      </div>

                      <div id="step-2">
                            <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Provinsi<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <select id="heard" class="form-control" name="provinsi" required>
                                <option value="">- Pilih Provinsi -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Kabupaten / Kota<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <select id="heard" class="form-control" name="kabupaten_kota" required>
                                <option value="">- Pilih Kabupaten / Kota -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Kecamatan<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <select id="heard" class="form-control" name="kecamatan" required>
                                <option value="">- Pilih Kecamatan -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Desa<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <select id="heard" class="form-control" name="desa" required>
                                <option value="">- Pilih Desa -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Alamat<span class="required">*</span></label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                              <textarea class="form-control" name="alamat"></textarea>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div id="step-3">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">Mulai Mengajar<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div class="control-group">
                                  <input type="date" class="form-control" name="mulai_mengajar">
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Status<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                                <input type="radio" class="flat" name="status" id="statusA" value="Aktif" checked="" required />&nbsp;Aktif&nbsp;&nbsp;&nbsp;
                                <input type="radio" class="flat" name="status" id="statusTA" value="Tidak Aktif" />&nbsp;Tidak Aktif
                              </p>
                            </div>
                          </div>

                       <!--<div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12">
                        Status<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div id="gender" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="status" value="laki-laki" name="no_induk"> &nbsp;&nbsp;&nbsp; Aktif &nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="status" value="perempuan" name="no_induk"> Tidak Aktif
                            </label>
                          </div>
                        </div>
                      </div>-->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Jabatan<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" name="jabatan" required>
                            <option value="">- Pilih Jabatan -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12">Ijazah<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="ijazah">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-4 col-xs-12">Surat Keputusan (SK) <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="sk">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12 col-xs-12">
                              <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-10">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                              </div>
                      </div>
                      </div>
                      <!--
                      <div id="step-4">
                        <h2 class="StepTitle">Step 4 Content</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                          in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                          in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                      </div>
                      -->

                    </div>
                
                     </form>
                    <!-- End SmartWizard Content -->
                   

               

        </div>
        </div>



 