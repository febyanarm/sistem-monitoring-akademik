<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Data Siswa - Sistem Monitoring Akademik</title>
<style type="text/css">
  .stepContainer{
    height: 10px !important;
  }
</style>
<div class="right_col" role="main">

               <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <center>
                    <h3 style="margin-bottom: 0; color:#26b99a; ">Formulir Data Siswa</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small>
                </center>
                  <div class="x_content">
                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Informasi Pribadi<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Informasi Orangtua / Wali<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <!--
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Data Kepegawaian<br />
                                              <small></small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Step 4 description</small>
                                          </span>
                          </a>
                        </li>-->
                      </ul>
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/input_siswa" method="POST">

                      <div id="step-1">
                      <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_induk">
                        Nomor Induk Siswa
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" id="no_induk" readonly="" value="<?php echo $generate_nis; ?>" class="form-control col-md-7 col-xs-12" name="no_induk">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_siswa">
                        Nama Siswa<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" id="nama_siswa" required="required" class="form-control col-md-7 col-xs-12" name="nama_siswa">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">
                        Tempat Lahir<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" id="tempat_lahir" required="required" class="form-control col-md-7 col-xs-12" name="tempat_lahir">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="date" class="form-control" name="tanggal_lahir">
                          </div>
                        </div>
                      </div>

                       <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Jenis Kelamin<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                                <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="laki-laki" checked="" required />&nbsp;Laki - Laki&nbsp;&nbsp;&nbsp;
                                <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="perempuan" />&nbsp;Perempuan
                              </p>
                            </div>
                          </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Agama<span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" required name="agama">
                            <option value="">- Pilih Agama -</option>
                            <option value="islam">Islam</option>
                            <option value="kristen">Kristen</option>
                            <option value="budha">Budha</option>
                            <option value="konghuchu">Konghuchu</option>
                            <option value="hindu">Hindu</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="file" class="form-control" placeholder="" name="userfile">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan Sebelumnya<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="pendidikan_sebelum">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gangguan / Kelainan<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="kelainan">
                          </div>
                        </div>
                      </div>
 <!--
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Date Mask</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" data-inputmask="'mask': '99/99/9999'">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
-->
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Saat Terjadi Kelainan<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="saat_terj_kelainan">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sebab<span class="required">*</span>
                        </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="sebab">
                          </div>
                        </div>
                      </div>
                      </div>
                      </div>


                      <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Provinsi<span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="control-group">
                              <select id="provinsi" class="form-control" name="provinsi" onchange="javascript:setProviceOrtu();" required>
                                <option value="">- Pilih Provinsi -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Kabupaten / Kota<span class="required">*</span></label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="control-group">
                              <select id="kabupaten_kota" class="form-control" name="kabupaten_kota" onchange="javascript:setKotOrtu();" required>
                                <option value="">- Pilih Kabupaten / Kota -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Kecamatan<span class="required">*</span></label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="control-group">
                              <select id="kecamatan" class="form-control" name="kecamatan" onchange="javascript:setKecOrtu();" required>
                                <option value="">- Pilih Kecamatan -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Desa<span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="control-group">
                              <select id="desa" class="form-control" name="desa" onchange="javascript:setDesOrtu();" required>
                                <option value="">- Pilih Desa -</option>
                                <option value="press">Islam</option>
                                <option value="net">Kristen</option>
                                <option value="mouth">Budha</option>
                              </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="heard">Alamat<span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="control-group">
                              <textarea class="form-control" name="alamat_siswa" id="alamat_siswa" oninput="javascript:setAlmtOrtu();"></textarea>
                              </div>
                            </div>
                          </div>
                    </div><!-- row -->
                    </div>
                    </div>
                      </div>

                      <div id="step-2">
                            <!-- ======================== data orangtua =========================-->
                        <div class="col-md-12 col-xs-12">
                        <div class="x_panel" style="">
                            <div class="x_title">
                              <h2><b>2</b>| Data Orangtua Siswa</h2>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                
                              </ul>
                               <!-- garis --><div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                        <div class="row">
                                <div class="form-group">
                                  <label class=" col-md-2 col-sm-2 col-xs-12" for="nis">
                                  Sudah pernah mendaftar ?
                                  </label>
                                  <div class="col-md-1 col-sm-1 col-xs-12">
                                    <label>
                                        <input type="checkbox" class="js-switch" checked="checked" id="sudahPernah" onClick="javascript:dataOrtu();"/>
                                        </label>
                                  </div>
                                  <div class="col-md-1 col-sm-1 col-xs-12 ">
                                  <button type="button" style="margin-left:-35px;" class="btn btn-info" href="#myModal" data-toggle="modal" data-target="#myModal"><b>Cari Data</b></button>
                                  </div> 
                                </div>
                                <br>
                           <div class="col-md-6 col-xs-12">
                                <input type="hidden" id="first-name" required="required" value="<?php echo $getid_ortu; ?>" class="form-control" name="id_orangtua">
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nis">
                                  Nama Ayah <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="namaayah" required="required" class="form-control col-md-7 col-xs-12" name="nama_ayah" readonly="">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nama-siswa">
                                  Nama Ibu <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="namaibu" required="required" class="form-control col-md-7 col-xs-12" name="nama_ibu" readonly="">
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tempat-lahir">
                                  Pekerjaan Ayah <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="pekerjaanayah" required="required" class="form-control col-md-7 col-xs-12" name="pekerjaan_ayah" readonly="">
                                  </div>
                                </div>
                               
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nis">
                                  Pekerjaan Ibu <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="pekerjaanibu" required="required" class="form-control col-md-7 col-xs-12" name="pekerjaan_ibu" readonly="">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tempat-lahir">
                                  Nomor Telp / HP <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="telp_ortu" class="form-control col-md-7 col-xs-12" name="telp_ortu" required="" readonly="">
                                  </div>
                                </div>
                               </div>

                               <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nama-siswa">
                                  Alamat <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <label>
                                        <input type="checkbox" class="js-switch" checked id="alamat_ortu" onClick="javascript:alamatOrtu();" disabled="" /> Sama Dengan Siswa
                                      </label>
                                  </div>
                                </div>
                            
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Provinsi<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select class="form-control" id="prov_ortu" required readonly=""  name="prov_ortu">
                                      <option value="">- Pilih Provinsi -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Kabupaten / Kota<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select class="form-control" id="kot_ortu" required readonly="" name="kot_ortu">
                                      <option value="">- Pilih Kabupaten / Kota -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Kecamatan<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select class="form-control" id="kec_ortu" required readonly="" name="kec_ortu">
                                      <option value="">- Pilih Kecamatan -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Desa<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select class="form-control" id="des_ortu" required readonly="" name="des_ortu">
                                      <option value="">- Pilih Desa -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Alamat<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <textarea class="form-control" id="almt_ortu" readonly="" name="alamat_orangtua"></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  <!-- ======================== data orangtua =========================-->

                  <!-- ======================== data wali =========================-->
                        <div class="col-md-12 col-xs-12">
                          <div class="x_panel" style="">
                            <div class="x_title">
                              <h2><b>3</b>| Data Wali Siswa<small>(Opsional)</small></h2>
                              
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                              </ul>
                               <!-- garis --><div class="clearfix"></div>
                            </div>
                            <div class="x_content" style="display: none;">
                            <input type="hidden" id="first-name" value="<?php echo $getid_wali; ?>" class="form-control" name="id_wali"> 
                             <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nis">
                                  Nama Wali
                                  </label>
                               <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="nama_wali" class="form-control col-md-7 col-xs-12" name="nama_wali">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tempat-lahir">
                                  Pekerjaan Wali
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="pekerjaan_wali" class="form-control col-md-7 col-xs-12" name="pekerjaan_wali">
                                  </div>
                                </div>
                               
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tempat-lahir">
                                  Nomor Telp / HP
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="telp_wali" class="form-control col-md-7 col-xs-12" name="telp_wali">
                                  </div>
                                </div>
                               
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nama-siswa">
                                  Alamat <span class="required">*</span>
                                  </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <label>
                                        <input type="checkbox" class="js-switch" checked id="alamat_wali" onClick="javascript:alamatWali();"/> Sama Dengan Siswa
                                      </label>
                                  </div>
                                </div>
                            
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Provinsi</label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select id="prov_wali" class="form-control" required readonly="" name="prov_wali">
                                      <option value="">- Pilih Provinsi -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Kabupaten / Kota</label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select id="kot_wali" class="form-control" required readonly="" name="kot_wali">
                                      <option value="">- Pilih Kabupaten / Kota -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Kecamatan</label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select id="kec_wali" class="form-control" required readonly="" name="kec_wali">
                                      <option value="">- Pilih Kecamatan -</option>
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Desa</label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <select id="des_wali" class="form-control" required readonly="" name="des_wali">
                                      <option value="">- Pilih Desa -</option> 
                                      <option value="press">Islam</option>
                                      <option value="net">Kristen</option>
                                      <option value="mouth">Budha</option>
                                    </select>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="heard">Alamat</label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="control-group">
                                    <textarea  id="almt_wali" class="form-control" readonly="" name="alamat_wali"></textarea>
                                    </div>
                                  </div>
                                </div>
                                
                            </div>
                          </div>
                        </div>
                  <!-- ======================== data wali =========================-->
                  <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
                      </div>

                      <div id="step-3">
                          
                      </div>

                      <div id="step-4">
            

                    </div>
                
                     </form>
                    <!-- End SmartWizard Content -->
                   

               

        </div>
<!-- ============================================= form ==================================-->
       <!-- ======================== data siswa =========================-->
        
          </form>
        </div>
        </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Pilih Mahasiswa</h4></center>
                    </div>
                    <div class="modal-body">
            <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/siswa/getdata_siswaAll"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">Nomor Induk Siswa</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="agama"  data-sortable="true">Agama</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Pendidikan Sebelum</th>
                                  <th data-field="kelainan" data-sortable="true">Kelainan</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Nama Ayah</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_ortu as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td>
                              <img id="foto" src="<?php echo base_url().'assets/images/siswa/'.$row->foto ?>" style="height: 100px; width: auto;">
                              </td>
                              <td><?php echo $row->no_induk; ?></td>
                              <td><?php echo $row->nama_siswa; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->agama; ?></td>
                              <td><?php echo $row->pendidikan_sebelum; ?></td>
                              <td><?php echo $row->kelainan; ?></td>
                              <td><?php echo $row->nama_ayah; ?></td>
                              <td>
                              <form action="<?php echo base_url();?>index.php/Admin/update_merk" method="POST" style="display: inline !important;">
                              <input type="hidden" value="<?php echo $row->no_induk; ?>" name="nopolisi">
                              <button type="submit" class="btn btn-success" value="Update" name="btnsubmit" title="Ubah"><i class="fa fa-edit" aria-hidden="true"></i></button><i class="" aria-hidden="true">
                              </form>
                              <form action="<?php echo base_url();?>index.php/Admin/hapus_merk" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="iderk" value="<?php echo $row->no_induk; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                    </div>
                </div>
            </div>
        </div>


                     
<script type="text/javascript">
//=======================================part orangtua ==================================================//


function setProviceOrtu(){
    if(document.getElementById('alamat_ortu').checked==true){
      document.getElementById('prov_ortu').value = document.getElementById('provinsi').value;
      }else{
      document.getElementById('prov_ortu').value =  "";
    }


    if(((document.getElementById('nama_wali').value != "")||(document.getElementById('pekerjaan_wali').value != "")) && (document.getElementById('alamat_wali').checked==true)){
      document.getElementById('prov_wali').value = document.getElementById('provinsi').value;
      }else{
      document.getElementById('prov_wali').value =  "";
    }
  }

  function setKotOrtu(){
    if(document.getElementById('alamat_ortu').checked==true){
      document.getElementById('kot_ortu').value = document.getElementById('kabupaten_kota').value;
      }else{
      document.getElementById('kot_ortu').value =  "";
    }


    if(((document.getElementById('nama_wali').value != "")||(document.getElementById('pekerjaan_wali').value != "")) && (document.getElementById('alamat_wali').checked==true)){
      document.getElementById('kot_wali').value = document.getElementById('kabupaten_kota').value;
      }else{
      document.getElementById('kot_wali').value =  "";
    }
  }

function setKecOrtu(){
    if(document.getElementById('alamat_ortu').checked==true){
      document.getElementById('kec_ortu').value = document.getElementById('kecamatan').value;
      }else{
      document.getElementById('kec_ortu').value =  "";
    }


    if(((document.getElementById('nama_wali').value != "")||(document.getElementById('pekerjaan_wali').value != "")) && (document.getElementById('alamat_wali').checked==true)){
      document.getElementById('kec_wali').value = document.getElementById('kecamatan').value;
      }else{
      document.getElementById('kec_wali').value =  "";
    }
  }

  function setDesOrtu(){
    if(document.getElementById('alamat_ortu').checked==true){
      document.getElementById('des_ortu').value = document.getElementById('desa').value;
      }else{
      document.getElementById('des_ortu').value =  "";
    }


    if(((document.getElementById('nama_wali').value != "")||(document.getElementById('pekerjaan_wali').value != "")) && (document.getElementById('alamat_wali').checked==true)){
      document.getElementById('des_wali').value = document.getElementById('desa').value;
      }else{
      document.getElementById('des_wali').value =  "";
    }
  }

  function setAlmtOrtu(){
    if(document.getElementById('alamat_ortu').checked==true){
      document.getElementById('almt_ortu').value = document.getElementById('alamat_siswa').value;
      }else{
      document.getElementById('almt_ortu').value =  " ";
    }


    if(((document.getElementById('nama_wali').value != "")||(document.getElementById('pekerjaan_wali').value != "")) && (document.getElementById('alamat_wali').checked==true)){
      document.getElementById('almt_wali').value = document.getElementById('alamat_siswa').value;
      }else{
      document.getElementById('almt_wali').value =  "";
    }
  }
function alamatOrtu(){
  if(document.getElementById('alamat_ortu').checked==true){
    document.getElementById('prov_ortu').value = document.getElementById('provinsi').value;
    document.getElementById('kot_ortu').value = document.getElementById('kabupaten_kota').value;
    document.getElementById('kec_ortu').value = document.getElementById('kecamatan').value;
    document.getElementById('des_ortu').value = document.getElementById('desa').value;
    document.getElementById('almt_ortu').value =  document.getElementById('alamat_siswa').value;
    document.getElementById('prov_ortu').setAttribute('readonly','readonly');
    document.getElementById('kot_ortu').setAttribute('readonly','readonly');
    document.getElementById('kec_ortu').setAttribute('readonly','readonly');
    document.getElementById('des_ortu').setAttribute('readonly','readonly');
    document.getElementById('almt_ortu').setAttribute('readonly','readonly');
    }else{
    document.getElementById('prov_ortu').removeAttribute('readonly');
    document.getElementById('kot_ortu').removeAttribute('readonly');
    document.getElementById('kec_ortu').removeAttribute('readonly');
    document.getElementById('des_ortu').removeAttribute('readonly');
    document.getElementById('almt_ortu').removeAttribute('readonly');
    document.getElementById('almt_ortu').value =  " ";
    document.getElementById('prov_ortu').value = "";
    document.getElementById('kot_ortu').value = "";
    document.getElementById('kec_ortu').value = "";
    document.getElementById('des_ortu').value = "";
  }
  }
//======================================= END OF part orangtua ==================================================//


 function dataOrtu(){
  if(document.getElementById('sudahPernah').checked==true){
    document.getElementById('namaayah').setAttribute('readonly','readonly');
    document.getElementById('namaibu').setAttribute('readonly','readonly');
    document.getElementById('pekerjaanayah').setAttribute('readonly','readonly');
    document.getElementById('pekerjaanibu').setAttribute('readonly','readonly');
    document.getElementById('telp_ortu').setAttribute('readonly','readonly');
   document.getElementById('alamat_ortu').setAttribute('disabled','disabled');
    }else{
    document.getElementById('namaayah').removeAttribute('readonly');
    document.getElementById('namaibu').removeAttribute('readonly');
    document.getElementById('pekerjaanayah').removeAttribute('readonly');
    document.getElementById('pekerjaanibu').removeAttribute('readonly');
    document.getElementById('telp_ortu').removeAttribute('readonly');
   document.getElementById('alamat_ortu').removeAttribute('disabled');
  }
}
//=======================================part wali siswa ==================================================//
function alamatWali(){
  if(document.getElementById('alamat_wali').checked==true){
    document.getElementById('prov_wali').value = document.getElementById('provinsi').value;
    document.getElementById('kot_wali').value = document.getElementById('kabupaten_kota').value;
    document.getElementById('kec_wali').value = document.getElementById('kecamatan').value;
    document.getElementById('des_wali').value = document.getElementById('desa').value;
    document.getElementById('almt_wali').value =  document.getElementById('alamat_siswa').value;
    document.getElementById('prov_wali').setAttribute('readonly','readonly');
    document.getElementById('kot_wali').setAttribute('readonly','readonly');
    document.getElementById('kec_wali').setAttribute('readonly','readonly');
    document.getElementById('des_wali').setAttribute('readonly','readonly');
    document.getElementById('almt_wali').setAttribute('readonly','readonly');
    }else{
    document.getElementById('prov_wali').removeAttribute('readonly');
    document.getElementById('kot_wali').removeAttribute('readonly');
    document.getElementById('kec_wali').removeAttribute('readonly');
    document.getElementById('des_wali').removeAttribute('readonly');
    document.getElementById('almt_wali').removeAttribute('readonly');
    document.getElementById('prov_wali').value =  "";
    document.getElementById('kot_wali').value = "";
    document.getElementById('kec_wali').value = "";
    document.getElementById('des_wali').value = "";
    document.getElementById('almt_wali').value = " ";
  }
  }
//=======================================END OF part wali siswa ==================================================//
</script>