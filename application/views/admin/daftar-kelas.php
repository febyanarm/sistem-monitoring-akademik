<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Data Mata Pelajaran - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Kelas</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">

               
<!-- ============================================= form ==================================-->
        
       <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
             <!-- garis --><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel collapse-div" style="background: darkseagreen;">
                <a class="collapse-link" style="cursor: pointer;">
                  <div class="x_title">
                    <h2>Tambah Data Kelas <small>(klik di sini untuk menambahkan data)</small></h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><i class="fa fa-chevron-up"></i></li>
                    </ul> -->
              <!-- garis --><div class="clearfix"></div>
              </a>
                  </div>
                  <div class="x_content" style="display: none;">
                    <br />
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/input_kelas" method="POST">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                         ID kelas
                          </label>
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" id="id_mapel" readonly="" value="<?php echo $getid_kelas; ?>" class="form-control col-md-7 col-xs-12" name="id_kelas">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                          Nama Kelas<span class="required">*</span>
                          </label>
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" required="required" placeholder="Cth : Abjad, Inisial atau Nama Benda" class="form-control col-md-7 col-xs-12" name="nama_kelas">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-4 col-xs-12" for="heard">Tingkat Kelas<span class="required">*</span></label>
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="control-group">
                            <select id="heard" class="form-control" required name="tingkat_kelas">
                              <option value="">- Pilih Tingkat Kelas -</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                            </select>
                            </div>
                          </div>
                        </div>
                   <!-- ============================================= form ==================================-->
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/Traffic/data_kelas"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <th data-field="state" data-checkbox="true" >Item ID</th>
                                  <th data-field="id" data-sortable="true">ID Kelas</th>
                                  <th data-field="name"  data-sortable="true">Nama Kelas</th>
                                  <th data-field="grade"  data-sortable="true">Tingkat</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_kelas as $row): ?>
                            <tr>

                              <!--<td><?php echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->id_kelas ?></td>
                              <td><?php echo $row->id_kelas ?></td>
                              <td><?php echo $row->nama_kelas; ?></td>
                              <td><?php echo $row->tingkat_kelas; ?></td>
                              <td>
                              <form action="<?php echo base_url();?>index.php/Admin/hapus_kelas" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="iderk" value="<?php echo $row->id_kelas; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>

            