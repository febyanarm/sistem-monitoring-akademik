<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Kelola Kegiatan Belajar - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Indeks Pencapaian Kompetensi</h3>
                    <hr style="margin-top: 0; ">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active">
                          <a href="#tab_content1" nama-mapel="" id="home-tab" class="mytab" role="tab" data-toggle="tab" aria-expanded="true">
                            <b>BERANDA</b>
                          </a>
                        </li>
                        <?php foreach ($getdata_mapel as $row): ?>
                        <li role="presentation" class="">
                        <input type="hidden" name="" id="<?php echo"ini"; echo $row->id_mapel; ?>" value="<?php echo $row->id_mapel; ?>">
                          <a href="<?php echo '#';echo $row->id_mapel; ?>" nama-mapel="<?php echo $row->nama_mapel; ?>" onclick="pilihMapel(getElementById('<?php echo"ini"; echo $row->id_mapel; ?>').value);" id="profile-tab" class="mytab" role="tab" data-toggle="tab" aria-expanded="true">
                            <b><?php echo $row->alias; ?></b>
                          </a>
                        </li>
                          <?php endforeach ?>
                      </ul>

                      <div id="myTabContent" class="tab-content">

                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          ini beranda
                        </div>

                        <?php foreach ($getdata_mapel as $row1): ?>
                        <div role="tabpanel" class="tab-pane fade" id="<?php echo $row1->id_mapel; ?>" aria-labelledby="profile-tab">

                          <table data-toggle='table' data-url='<?php echo base_url(); ?>index.php/admin/loadData'  data-show-refresh='true' data-show-toggle='true' data-show-columns='true' data-search='true' data-select-item-name='toolbar1' data-pagination='true' data-sort-name='name' data-sort-order='desc'>
                              <thead >
                              <tr>
                                  <!--<th data-field='state' data-checkbox='true' >Item ID</th>-->
                                  <th data-field='mapel'  data-sortable='true'>Mata Pelajaran</th>
                                  <th data-field='tingkat'  data-sortable='true'>Tingkat Kelas</th>
                                  <th data-field='nokd'  data-sortable='true'>No. KD</th>
                                  <th data-field='ket'  data-sortable='true'>Ketrangan</th>
                                  <th data-field='lihat'  data-sortable='true'>Lihat / Tambah IPK</th>
                              </tr>
                              </thead>
                              <tbody id='dynamic<?php echo $row1->id_mapel; ?>'>
                                
                              </tbody>
                              </table>
                        </div>
                      <?php endforeach ?>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Tambah Data IPK</h4></center>
                    </div>
                    <div class="modal-body">
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_IPK" method="POST">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                            ID Kompetensi Dasar
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="id_kd" readonly="" class="form-control col-md-7 col-xs-12" name="id_kd">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_mapel">
                            No. Kompetensi Dasar<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text"  id="no_kd" readonly=""  class="form-control col-md-7 col-xs-12" name="no_kd">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_mapel">
                            Keterangan<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text"  id="keterangan" readonly="" class="form-control col-md-7 col-xs-12" >
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_mapel">
                            Keterangan IPK<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text"  id="keterangan" required="" class="form-control col-md-7 col-xs-12" name="ket_ipk">
                            </div>
                          </div>

                          <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                          </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

//            jika dipilih, nim akan masuk ke input dan modal di tutup
             $(document).on('click', '#pilih', function (e) {
              document.getElementById('id_kd').value =  $(this).attr('data-kd');
              document.getElementById('no_kd').value =  $(this).attr('data-nomor');
              document.getElementById('keterangan').value =  $(this).attr('data-nama');
                $('#myModal').modal('hide');
            });
        </script>
