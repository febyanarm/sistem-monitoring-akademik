<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Formulir Data Siswa</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">

               
<!-- ============================================= form ==================================-->
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/siswa/tambah_siswa" method="POST">
       <!-- ======================== data siswa =========================-->
        <div class="col-md-6 col-xs-12">
             <!-- garis --><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><b>1</b>| Data Orangtua Siswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link  pull-right"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
              <!-- garis --><div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="no_induk">
                        Nomor Induk Siswa
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="no_induk" readonly="" value="<?php echo $generate_nis; ?>" class="form-control col-md-7 col-xs-12" name="no_induk">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama_siswa">
                        Nama Siswa<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="nama_siswa" required="required" class="form-control col-md-7 col-xs-12" name="nama_siswa">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tempat_lahir">
                        Tempat Lahir<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="tempat_lahir" required="required" class="form-control col-md-7 col-xs-12" name="tempat_lahir">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Tanggal Lahir<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="date" class="form-control" name="tanggal_lahir">
                          </div>
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Jenis Kelamin<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div id="gender" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="jenis_kelamin" value="laki-laki" name="no_induk"> &nbsp; Pria &nbsp;
                            </label>
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="jenis_kelamin" value="perempuan" name="no_induk"> Wanita
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Agama<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" required name="agama">
                            <option value="">- Pilih Agama -</option>
                            <option value="islam">Islam</option>
                            <option value="kristen">Kristen</option>
                            <option value="budha">Budha</option>
                            <option value="konghuchu">Konghuchu</option>
                            <option value="hindu">Hindu</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Pendidikan Sebelumnya<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="pendidikan_sebelum">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Gangguan / Kelainan<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="kelainan">
                          </div>
                        </div>
                      </div>
 <!--
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Date Mask</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" data-inputmask="'mask': '99/99/9999'">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
-->
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Saat Terjadi Kelainan<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="saat_terj_kelainan">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Sebab<span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                              <input type="text" class="form-control" name="sebab">
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <h2>Alamat Siswa</h2>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Provinsi<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" name="provinsi" required>
                            <option value="">- Pilih Provinsi -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kabupaten / Kota<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" name="kabupaten_kota" required>
                            <option value="">- Pilih Kabupaten / Kota -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kecamatan<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" name="kecamatan" required>
                            <option value="">- Pilih Kecamatan -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Desa<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="heard" class="form-control" name="desa" required>
                            <option value="">- Pilih Desa -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Alamat<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <textarea class="form-control" name="alamat_siswa"></textarea>
                          </div>
                        </div>
                      </div>
                      
                   <!-- ============================================= form ==================================-->
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!-- ======================== data siswa =========================-->

        <!-- ======================== data orangtua =========================-->
              <div class="col-md-6 col-xs-12">
                <div class="x_panel" style="">
                  <div class="x_title">
                    <h2><b>2</b>| Data Orangtua Siswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                     <!-- garis --><div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <input type="hidden" id="first-name" required="required" value="<?php echo $getid_ortu; ?>" class="form-control" name="id_orangtua"> 
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nis">
                        Nama Ayah <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="nama_ayah">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama-siswa">
                        Nama Ibu <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="nama_ibu">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tempat-lahir">
                        Pekerjaan Ayah <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="pekerjaan_ayah">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nis">
                        Pekerjaan Ibu <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="pekerjaan_ibu">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama-siswa">
                        Alamat <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <label>
                              <input type="checkbox" class="js-switch" checked id="alamat_ortu" onClick="javascript:alamatOrtu();"/> Sama Dengan Siswa
                            </label>
                        </div>
                      </div>
                  
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Provinsi<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select class="form-control" id="prov_ortu" required disabled=""  name="provinsi">
                            <option value="">- Pilih Provinsi -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kabupaten / Kota<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select class="form-control" id="kot_ortu" required disabled="" name="kabupaten_kota">
                            <option value="">- Pilih Kabupaten / Kota -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kecamatan<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select class="form-control" id="kec_ortu" required disabled="" name="kecamatan">
                            <option value="">- Pilih Kecamatan -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Desa<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select class="form-control" id="des_ortu" required disabled="" name="desa">
                            <option value="">- Pilih Desa -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Alamat<span class="required">*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <textarea class="form-control" id="almt_ortu" disabled="" name="alamat_orangtua"></textarea>
                          </div>
                        </div>
                      </div>
                      
                  </div>
                </div>
              </div>
        <!-- ======================== data orangtua =========================-->

        <!-- ======================== data wali =========================-->
 <div class="col-md-6 col-xs-12">
                <div class="x_panel" style="">
                  <div class="x_title">
                    <h2><b>3</b>| Data Wali Siswa<small>(Opsional)</small></h2>
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                      </li>
                    </ul>
                     <!-- garis --><div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: none;">
                  <input type="hidden" id="first-name" value="<?php echo $getid_wali; ?>" class="form-control" name="id_wali"> 
                   <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nis">
                        Nama Wali
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="nama_wali">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tempat-lahir">
                        Pekerjaan Wali
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="pekerjaan_wali">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="nama-siswa">
                        Alamat <span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <label>
                              <input type="checkbox" class="js-switch" checked id="alamat_wali" onClick="javascript:alamatWali();"/> Sama Dengan Siswa
                            </label>
                        </div>
                      </div>
                  
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Provinsi</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="prov_wali" class="form-control" required disabled="" name="provinsi">
                            <option value="">- Pilih Provinsi -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kabupaten / Kota</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="kot_wali" class="form-control" required disabled="" name="kabupaten_kota">
                            <option value="">- Pilih Kabupaten / Kota -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Kecamatan</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="kec_wali" class="form-control" required disabled="" name="kecamatan">
                            <option value="">- Pilih Kecamatan -</option>
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Desa</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <select id="des_wali" class="form-control" required disabled="" name="desa">
                            <option value="">- Pilih Desa -</option> 
                            <option value="press">Islam</option>
                            <option value="net">Kristen</option>
                            <option value="mouth">Budha</option>
                          </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="heard">Alamat</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="control-group">
                          <textarea  id="almt_wali" class="form-control" disabled="" name="alamat_wali"></textarea>
                          </div>
                        </div>
                      </div>
                      
                  </div>
                </div>
              </div>
        <!-- ======================== data wali =========================-->
              <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                          <button class="btn btn-primary" type="button">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
          </form>
        </div>
        </div>



                     
<script type="text/javascript">

function alamatOrtu(){
  if(document.getElementById('alamat_ortu').checked==true){
    document.getElementById('prov_ortu').setAttribute('disabled','disabled');
    document.getElementById('kot_ortu').setAttribute('disabled','disabled');
    document.getElementById('kec_ortu').setAttribute('disabled','disabled');
    document.getElementById('des_ortu').setAttribute('disabled','disabled');
    document.getElementById('almt_ortu').setAttribute('disabled','disabled');
    }else{
    document.getElementById('prov_ortu').removeAttribute('disabled');
    document.getElementById('kot_ortu').removeAttribute('disabled');
    document.getElementById('kec_ortu').removeAttribute('disabled');
    document.getElementById('des_ortu').removeAttribute('disabled');
    document.getElementById('almt_ortu').removeAttribute('disabled');
  }
  }
function alamatWali(){
  if(document.getElementById('alamat_wali').checked==true){
    document.getElementById('prov_wali').setAttribute('disabled','disabled');
    document.getElementById('kot_wali').setAttribute('disabled','disabled');
    document.getElementById('kec_wali').setAttribute('disabled','disabled');
    document.getElementById('des_wali').setAttribute('disabled','disabled');
    document.getElementById('almt_wali').setAttribute('disabled','disabled');
    }else{
    document.getElementById('prov_wali').removeAttribute('disabled');
    document.getElementById('kot_wali').removeAttribute('disabled');
    document.getElementById('kec_wali').removeAttribute('disabled');
    document.getElementById('des_wali').removeAttribute('disabled');
    document.getElementById('almt_wali').removeAttribute('disabled');
  }
  }
</script>

               