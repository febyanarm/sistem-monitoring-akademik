<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Data Mata Pelajaran - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Mata Pelajaran</h3>
                    <small><b>Tips !</b> Gunakan Tombol <b><i>Tab</i></b> Untuk Beralih Kolom Isian Dengan Lebih Mudah</small></center>
                    <hr style="margin-top: 0; ">
                    
               
<!-- ============================================= form ==================================-->
       <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
             <!-- garis --><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="alert alert-info" role="alert">
              <h3>Harap diperhatikan !</h3>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <br>Mengacu pada kurikulum 2013 Revisi tahun 2017, penilaian sikap dengan KI 1 dan 2 hanya pada mata pelajaran <b>PENDIDIKAN AGAMA</b> dan <b>PENDIDIKAN KEWARGANEGARAAN</b>
                        <br>Agar dapat memasukkan KI 1 dan 2 pada tab RPP-Kompetensi Dasar, ketika menambahkan mata pelajaran tersebut pada kolom 'Ditangani oleh:' pilih kondisi 'Guru Mata Pelajaran'
                        <br>Terima Kasih
                  </div>
                <div class="x_panel collapse-div" style="background: darkseagreen;">
                <a class="collapse-link" style="cursor: pointer;">
                  <div class="x_title">
                    <h2>Tambah Data Mata Pelajaran <small>(klik di sini untuk menambahkan data)</small></h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><i class="fa fa-chevron-up"></i></li>
                    </ul> -->
              <!-- garis --><div class="clearfix"></div>
              </a>
                  </div>
                  <div class="x_content" style="display: none;">
                    <br />
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_mapel" method="POST">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                            ID Mata Pelajaran
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" readonly="" value="<?php echo $getid_mapel; ?>" class="form-control col-md-7 col-xs-12" name="id_mapel">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Nama Mata Pelajaran<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="nama_mapel" placeholder="Misalkan : Ilmu Pengetahuan Alam, Ilmu Pengetahuan Sosial, dll">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru" >
                            Alias<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="nama_mapel" placeholder="Misalkan : IPA, IPS, dll">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Ditangani Oleh<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <select id="heard" class="form-control" required name="author">
                                <option value="">- Pilih Guru -</option>
                                <option value="Guru Kelas">Guru Kelas / Wali Kelas</option>
                                <option value="Guru Mata Pelajaran">Guru Mata Pelajaran (AGAMA / PKn / Bahasa / Olahraga)</option>
                                <option value="Guru Ekstrakurikuler">Guru Ekstrakurikuler</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                            <button class="btn btn-success" type="reset">Urungkan</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/Traffic/data_mapel"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="id" data-sortable="true">Id Mata Pelajaran</th>
                                  <th data-field="nama"  data-sortable="true">Mata Pelajaran</th>
                                  <th data-field="author"  data-sortable="true">Ditangani Oleh</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_mapel as $row): ?>
                            <tr>

                              <!--<td><?php echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->id_mapel; ?></td>
                              <td><?php echo $row->nama_mapel; ?></td>
                              <td><?php echo $row->author; ?></td>
                              <td>

                             <button type="button" class="edit btn btn-success" href="#myModal" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row->id_mapel; ?>" data-nama="<?php echo $row->nama_mapel; ?>" data-auth="<?php echo $row->author; ?>"><i class="fa fa-edit" aria-hidden="true"></i></button>
                           
                              <form action="<?php echo base_url();?>index.php/admin/hapus_mapel" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="hapus_mapel" value="<?php echo $row->id_mapel; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>


 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Ubah Data Mata Pelajaran</h4></center>
                    </div>
                    <div class="modal-body">
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/edit_mapel" method="POST">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                            ID Mata Pelajaran
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text" id="id_mapel_edit" readonly="" value="<?php echo $getid_mapel; ?>" class="form-control col-md-7 col-xs-12" name="id_mapel1">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru1">
                            Nama Mata Pelajaran<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <input type="text"  id="nama_mapel"  required="required" class="form-control col-md-7 col-xs-12" name="nama_mapel1">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                            Ditangani Oleh<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <select id="author" class="form-control" required name="author1">
                                <option value="">- Pilih Guru -</option>
                                <option value="Guru Kelas">Guru Kelas / Wali Kelas</option>
                                <option value="Guru Mata Pelajaran">Guru Bahasa / Agama / Olahraga</option>
                                <option value="Guru Ekstrakurikuler">Guru Ekstrakurikuler</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                          </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    
</div>

<script type="text/javascript">

//            jika dipilih, nim akan masuk ke input dan modal di tutup
          $(document).on('click', '.edit', function (e) {
                document.getElementById("id_mapel_edit").value = $(this).attr('data-id');
                document.getElementById("nama_mapel").value = $(this).attr('data-nama');
                document.getElementById("author").value = $(this).attr('data-auth');
            });
            
</script>