<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Kelola Kegiatan Belajar - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Kelola Kegiatan Belajar Mengajar</h3></center>
                    <hr style="margin-top: 0; ">



<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" class="mytab" role="tab" data-toggle="tab" aria-expanded="true"><b>Guru Kelas / Wali Kelas</b></a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" class="mytab"  id="profile-tab" data-toggle="tab" aria-expanded="false">Guru Mata Pelajaran Khusus<small>(Agama, Penjaskes, dll)</small></a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/admin/kelola_kbm"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">Pilih</th>
                                  <th data-field="id" data-sortable="true">NIP</th>
                                  <th data-field="name"  data-sortable="true">Nama Guru</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Mulai Mengajar</th>
                                  <th data-field="kelainan" data-sortable="true">Telepon</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Status</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_dataGuruKosong as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td>
                                <button type="button" data-nip="<?php echo $row->no_induk_pegawai; ?>" data-nama="<?php echo $row->nama_guru; ?>" id="pilih" class="btn btn-default" href="#myModal" data-toggle="modal" data-target="#myModal"><b>Pilih</b></button>
                              </td>
                              <td><?php echo $row->no_induk_pegawai; ?></td>
                              <td><?php echo $row->nama_guru; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tgl_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->mulai_mengajar; ?></td>
                              <td><?php echo $row->telp; ?></td>
                              <td><?php echo $row->status; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/admin/kelola_kbm"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <!--<th data-field="state" data-checkbox="true" >Item ID</th>-->
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">NIP</th>
                                  <th data-field="name"  data-sortable="true">Nama Guru</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Mulai Mengajar</th>
                                  <th data-field="kelainan" data-sortable="true">Telepon</th>
                                  <th data-field="nama_ayah"  data-sortable="true">Status</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_calonGuruMP as $row): ?>
                            <tr>

                              <!--<td><?php //echo $row->idmerk; ?></td>-->
                              <td>
                                <button type="button" data-nip1="<?php echo $row->no_induk_pegawai; ?>" data-nama1="<?php echo $row->nama_guru; ?>" id="pilih1" class="btn btn-default" href="#myModal1" data-toggle="modal" data-target="#myModal1"><b>Pilih</b></button>
                              </td>
                              <td><?php echo $row->no_induk_pegawai; ?></td>
                              <td><?php echo $row->nama_guru; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tgl_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->mulai_mengajar; ?></td>
                              <td><?php echo $row->telp; ?></td>
                              <td><?php echo $row->status; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                        
                      </div>
                    </div>

                  </div>
                </div>
              </div>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Pilih Kelas</h4></center>
                    </div>
                    <div class="modal-body">
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_kbm" method="POST">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              ID Jadwal Mengajar
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text"  readonly="" value="<?php echo $getid_kbm; ?>" class="form-control col-md-7 col-xs-12" name="id_jadwal">
                                <input type="hidden"  readonly="" value="<?php echo $getid_login; ?>" class="form-control col-md-7 col-xs-12" name="id_login">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              NIP
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" value="" id="idguru" class="form-control col-md-7 col-xs-12" name="nip" readonly="">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              Nama Guru
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" value="" id="namaguru" class="form-control col-md-7 col-xs-12" name="namaguru" readonly="">
                              </div>
                            </div>

                          <div class="form-group">
                          <h4>Kelas Yang Tersedia </h4>
                          <hr>
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Pilih Kelas<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                              <div class="row">
                                 <?php foreach ($get_kelasOutWakel as $row1): ?>
                                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                     <input type="radio" class="flat" name="id_kelas" id="kelas" value="<?php echo $row1->id_kelas; ?>" required /><?php echo 'Kelas'; echo ' '; echo $row1->tingkat_kelas; echo ' '; echo $row1->nama_kelas; ?>
                                  </div>
                                 <?php endforeach ?>
                              </div>
                              </p>
                            </div>
                          </div>

                       <!-- ============================================= form ==================================-->
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                              <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Pilih Mata Pelajaran dan Kelas</h4></center>
                    </div>
                    <div class="modal-body">
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_kbmGMP" method="POST">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              ID Jadwal Mengajar
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text"  readonly="" value="<?php echo $getid_kbm; ?>" class="form-control col-md-7 col-xs-12" name="id_jadwal1">
                                <input type="hidden"  readonly="" value="<?php echo $getid_login; ?>" class="form-control col-md-7 col-xs-12" name="id_login1">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              NIP
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" value="" id="idguru1" class="form-control col-md-7 col-xs-12" name="nip" readonly="">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              Nama Guru
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" value="" id="namaguru1" class="form-control col-md-7 col-xs-12" name="namaguru" readonly="">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="nama_guru">
                              Pilih Mata Pelajaran<span class="required">*</span>
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <select id="heard" class="form-control" required name="mapel" onchange="ambilKelas(this.value);">
                                  <option value="">- Mata Pelajaran -</option>
                                 <?php foreach ($getdata_mapelGMP as $row2): ?>
                                  <option value="<?php echo $row2->id_mapel; ?>"><?php echo $row2->nama_mapel; ?></option>
                                 <?php endforeach ?>
                                </select>
                              </div>
                            </div>

                          <div class="form-group">
                          <h4>Kelas Yang Tersedia </h4>
                          <hr>
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Pilih Kelas<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                              <div class="row" id='daftarKelas'>
                                 
                              </div>
                              </p>
                            </div>
                          </div>

                       <!-- ============================================= form ==================================-->
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                              <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript">

             $(document).on('click', '#pilih1', function (e) {
              document.getElementById('idguru1').value =  $(this).attr('data-nip1');
              document.getElementById('namaguru1').value =  $(this).attr('data-nama1');
                $('#myModal').modal('hide');
            });
//            jika dipilih, nim akan masuk ke input dan modal di tutup
             $(document).on('click', '#pilih', function (e) {
              document.getElementById('idguru').value =  $(this).attr('data-nip');
              document.getElementById('namaguru').value =  $(this).attr('data-nama');
                $('#myModal').modal('hide');
            });
        </script>