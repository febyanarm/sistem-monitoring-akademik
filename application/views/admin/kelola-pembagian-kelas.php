<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Kelola Pembagian Kelas - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">



<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Kelola Pembagian Kelas</h3></center>
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" class="mytab" role="tab" data-toggle="tab" aria-expanded="true"><b>Siswa Baru</b></a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" class="mytab"  id="profile-tab" data-toggle="tab" aria-expanded="false">Siswa Pindahan</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab"  class="mytab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Tinggal Kelas</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                       <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/kelola_siswa_baru" method="POST">

                          <div class="form-group">
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="nama_guru">
                           Kelas<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <select id="heard" class="form-control" required name="kelas">
                                <option value="">- Pilih Kelas -</option>
                       <?php foreach ($get_kelasSatu as $key) : ?>
                                <option value="<?php echo $key->id_kelas;?>"><?php echo $key->tingkat_kelas; echo " - "; echo $key->nama_kelas;?></option>
                       <?php endforeach ?>
                              </select>
                            </div>
                          </div>
                          <table data-toggle="table" id="table" data-url="<?php echo base_url(); ?>index.php/admin/get_dataGuruKosong"  data-id-field="id" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-select-item-name="nis[]" data-sort-name="name" data-sort-order="desc" data-click-to-select="true">
                              <thead>
                              <tr>
                                  <th data-field="id" data-checkbox="true" >NIS</th>
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">NIS</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Jenis Ketunaan</th>
                                  <th data-field="kelainan" data-sortable="true">Saat Terjadinya Ketunaan</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_siswaBaru as $row): ?>
                            <tr>

                              <td>
                                <input type="checkbox" class="flat" data-value="<?php echo $row->no_induk; ?>" >
                              </td>
                              <td>
                               <img src="<?php echo base_url().'assets/images/siswa/'.$row->foto ?>" alt="..." style="width: 70px; height: auto;">
                              </td>
                              <td><?php echo $row->no_induk; ?></td>
                              <td><?php echo $row->nama_siswa; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->kelainan; ?></td>
                              <td><?php echo $row->saat_terj_kelainan; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-11">
                              <button type="submit" id="button" class="btn btn-success">Simpan</button>
                            </div>
                          </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/kelola_siswa_pindahan" method="POST">

                          <div class="form-group">
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="nama_guru">
                           Kelas<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <select id="heard" class="form-control" required name="kelas1">
                                <option value="">- Pilih Kelas -</option>
                       <?php foreach ($getdata_kelas as $key) : ?>
                                <option value="<?php echo $key->id_kelas;?>"><?php echo $key->tingkat_kelas; echo " - "; echo $key->nama_kelas;?></option>
                       <?php endforeach ?>
                              </select>
                            </div>
                          </div>
                          <table data-toggle="table" id="table" data-url="<?php echo base_url(); ?>index.php/admin/get_dataGuruKosong"  data-id-field="id" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-select-item-name="nis[]" data-sort-name="name" data-sort-order="desc" data-click-to-select="true">
                              <thead>
                              <tr>
                                  <th data-field="id" data-checkbox="true" >NIS</th>
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">NIS</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Jenis Ketunaan</th>
                                  <th data-field="kelainan" data-sortable="true">Saat Terjadinya Ketunaan</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_siswaPindahan as $row): ?>
                            <tr>

                              <td>
                                <input type="checkbox" class="flat" data-value="<?php echo $row->no_induk; ?>" >
                              </td>
                              <td>
                               <img src="<?php echo base_url().'assets/images/siswa/'.$row->foto ?>" alt="..." style="width: 70px; height: auto;">
                              </td>
                              <td><?php echo $row->no_induk; ?></td>
                              <td><?php echo $row->nama_siswa; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->kelainan; ?></td>
                              <td><?php echo $row->saat_terj_kelainan; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-11">
                              <button type="submit" id="button" class="btn btn-success">Simpan</button>
                            </div>
                          </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/kelola_siswa_pindahan" method="POST">

                          <table data-toggle="table" id="table" data-url="<?php echo base_url(); ?>index.php/admin/get_dataGuruKosong"  data-id-field="id" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-select-item-name="nis[]" data-sort-name="name" data-sort-order="desc" data-click-to-select="true">
                              <thead>
                              <tr>
                                  <th data-field="id" data-checkbox="true" >NIS</th>
                                  <th data-field="foto" data-sortable="true">Foto</th>
                                  <th data-field="id" data-sortable="true">NIS</th>
                                  <th data-field="name"  data-sortable="true">Nama Siswa</th>
                                  <th data-field="ttl" data-sortable="true">Tempat / Tanggal Lahir</th>
                                  <th data-field="jenis_kelamin" data-sortable="true">Jenis Kelamin</th>
                                  <th data-field="pendidikan_sebelum" data-sortable="true">Jenis Ketunaan</th>
                                  <th data-field="kelainan" data-sortable="true">Saat Terjadinya Ketunaan</th>
                              </tr>
                              </thead>
                              <?php foreach ($get_siswaTinggal as $row): ?>
                            <tr>

                              <td>
                                <input type="checkbox" class="flat" data-value="<?php echo $row->no_induk; ?>" >
                              </td>
                              <td>
                               <img src="<?php echo base_url().'assets/images/siswa/'.$row->foto ?>" alt="..." style="width: 70px; height: auto;">
                              </td>
                              <td><?php echo $row->no_induk; ?></td>
                              <td><?php echo $row->nama_siswa; ?></td>
                              <td><?php echo $row->tempat_lahir; echo", "; echo $row->tanggal_lahir; ?></td>
                              <td><?php echo $row->jenis_kelamin; ?></td>
                              <td><?php echo $row->kelainan; ?></td>
                              <td><?php echo $row->saat_terj_kelainan; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-11">
                              <button type="submit" id="button" class="btn btn-success">Simpan</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Pilih Kelas</h4></center>
                    </div>
                    <div class="modal-body">
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/tambah_kbm" method="POST">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              ID Jadwal Mengajar
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text"  readonly="" value="<?php echo $getid_kbm; ?>" class="form-control col-md-7 col-xs-12" name="id_jadwal">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              NIP
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text"  readonly="" id="idguru" class="form-control col-md-7 col-xs-12" name="nip">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-4 col-xs-12" for="no_induk">
                              Nama Guru
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text"  readonly="" id="namaguru" class="form-control col-md-7 col-xs-12" name="nama_guru">
                              </div>
                            </div>

                          <div class="form-group">
                          <h4>Kelas Yang Tersedia </h4>
                          <hr>
                            <label class="control-label col-md-3 col-sm-4 col-xs-12">
                              Pilih Kelas<span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <p>
                              <div class="row">
                                 <?php foreach ($get_kelasOutWakel as $row): ?>
                                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                     <input type="radio" class="flat" name="id_kelas" id="kelas" value="<?php echo $row->id_kelas; ?>" required /><?php echo 'Kelas'; echo ' '; echo $row->tingkat_kelas; echo ' '; echo $row->nama_kelas;?>
                                  </div>
                                 <?php endforeach ?>
                              </div>
                              </p>
                            </div>
                          </div>

                       <!-- ============================================= form ==================================-->
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                              <button class="btn btn-info" data-dismiss="modal" aria-label="Close">Batal</button>
                              <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript">

//            jika dipilih, nim akan masuk ke input dan modal di tutup
            $(document).on('click', '.pilih', function (e) {
                document.getElementById("idguru").value = document.getElementById("nip").value;
                document.getElementById("namaguru").value = document.getElementById("nama").value;
                $('#myModal').modal('hide');
            });

           /* var $table = $('#table'),
          $button = $('#button');

        $(function () {
            $button.click(function () {
                alert('getSelections: ' + JSON.stringify($table.bootstrapTable('getSelections')));
            });
        });*/
        </script>