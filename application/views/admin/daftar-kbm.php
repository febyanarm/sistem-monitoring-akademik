<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><title>Data Kegiatan Belajar - Sistem Monitoring Akademik</title>
<div class="right_col" role="main">
                    <center><h3 style="margin-bottom: 0; color:#26b99a; ">Data Kegiatan Belajar</h3></center>
                    <hr style="margin-top: 0; ">

               
<!-- ============================================= form ==================================-->
        
        <!-- ======================== data siswa =========================-->
        <div class="col-md-12 col-xs-12">
                <!-- form date pickers -->
                <div class="x_panel" style="">
                  <div class="x_content">
                      <div class="panel-body">
                          <table data-toggle="table" data-url="<?php echo base_url(); ?>index.php/Traffic/daftar_kbm"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                              <thead>
                              <tr>
                                  <th data-field="state" data-checkbox="true" >Item ID</th>
                                  <th data-field="id" data-sortable="true">ID Kelas</th>
                                  <th data-field="name"  data-sortable="true">Mata Pelajaran / Ekstrakurikuler</th>
                                  <th data-field="guru"  data-sortable="true">Nama Guru</th>
                                  <th data-field="grade"  data-sortable="true">Kelas</th>
                                  <th data-field="action" data-sortable="true">Aksi</th>
                              </tr>
                              </thead>
                              <?php foreach ($getdata_kbm as $row): ?>
                            <tr>

                              <!--<td><?php echo $row->idmerk; ?></td>-->
                              <td><?php echo $row->id_jadwal ?></td>
                              <td><?php echo $row->id_jadwal ?></td>
                              <td><?php echo $row->nama_mapel ?></td>
                              <td><?php echo $row->nama_guru; ?></td>
                              <td><?php echo $row->nama_kelas; ?></td>
                              <td>
                              <form action="<?php echo base_url();?>index.php/Admin/hapus_kelas" method="POST" style="display: inline !important;" style="display: inline-block;">
                              <input type="hidden" name="iderk" value="<?php echo $row->id_kelas; ?>">
                              <button type="submit" class="btn btn-info" value="Hapus" name="btnsubmit" title="Hapus"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                              </form>

                              </td>
                            </tr>
                          <?php endforeach ?>
                          </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>

