<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu <?php echo $role; ?></h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url();?>index.php/guru_kelas/dashboard"><i class="fa fa-home"></i>Beranda</a>
                  <li><a><i class="fa fa-edit"></i> PENILAIAN <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/guru_kelas/PH"><i class="fa fa-book"></i>Penilaian Mingguan</a>
                      <li><a href="<?php echo base_url();?>index.php/guru_kelas/PTS"><i class="fa fa-book"></i>Penilaian Tengah Semester</a>
                      <li><a href="<?php echo base_url();?>index.php/guru_kelas/PAS"><i class="fa fa-book"></i>Penilaian Akhir Semester</a>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url();?>index.php/guru_kelas/daftar_siswa"><i class="fa fa-book"></i>Siswa</a>
                  <li><a href="<?php echo base_url();?>index.php/guru_kelas/nilai_bermasalah"><i class="fa fa-exclamation-triangle"></i>Kelola Nilai Bermasalah</a>
                  </li>
                </ul>
              </div>
            </div>