<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu <?php echo $role; ?></h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-user"></i> Data Guru <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/admin/daftar_guru">Daftar Guru</a></li>
                      <li><a href="<?php echo base_url();?>index.php/admin/tambah_guru">Tambah Data Guru</a></li>
                    </ul>
                  </li>
                  </li>
                  <li><a><i class="fa fa-group"></i> Data Siswa <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/admin/daftar_siswa">Daftar Siswa</a></li>
                      <li><a href="<?php echo base_url();?>index.php/admin/tambah_siswa">Tambah Siswa</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url();?>index.php/admin/data_mapel"><i class="fa fa-book"></i>Data Mata Pelajaran</a>
                  </li>
                  <li><a><i class="fa fa-pencil-square-o"></i> Kelas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/admin/daftar_kelas">Data Kelas</a></li>
                      <li><a href="<?php echo base_url();?>index.php/admin/kelola_pembagian_kelas">Kelola Pembagian Kelas</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-calendar-o"></i> Data KBM <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/admin/daftar_kbm">Daftar KBM</a></li>
                      <li><a href="<?php echo base_url();?>index.php/admin/kelola_kbm">Kelola Data KBM</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-calendar-o"></i> RPP <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/admin/data_KD">Data Kompetensi Dasar</a></li>
                      <li><a href="<?php echo base_url();?>index.php/admin/data_IPK">Data Indkator Pencapaian Kompetensi</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url();?>index.php/admin/data_mapel"><i class="fa fa-exclamation-triangle"></i>Kelola Nilai Bermasalah</a>
                  </li>
                </ul>
              </div>
            </div>