<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Log In</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style1.css">
  <body>
  <div id="wrapper">

  <form name="login-form" class="login-form" action="<?php echo base_url(); ?>index.php/auth/cek_login" method="post">
  
    <div class="header">
    <center><h1>LOGIN</h1></center>
    </div>
  
    <div class="content">
    <input name="nama_user" type="text" class="input username" placeholder="Nama Pengguna" required />
    <input name="password" type="password" class="input password" placeholder="Kata Sandi" required />
    </div>
    <center>
    <div class="footer">
    <input type="reset" name="reset" value="Batal" class="button-reset" />
    <input type="submit" name="submit" value="Login" class="button" /> 
    </div>
    </center>
  
  </form>

</div>
<div class="gradient"></div>

  </body>
</html>