<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		/*$this->load->library('session');
		if ($this->session->userdata('id_user')=="" OR $this->session->userdata('role')!="Admin"){
			$this->session->unset_userdata('id_user');
			$this->session->unset_userdata('role');
			session_destroy();
			echo"Anda Belum Login";
			redirect('Auth/Index');
		}*/
		$this->load->database();
		$this->load->model('Admin');
		$this->load->model('Siswa_model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['isi'] = $this->load->view('data-siswa', NULL, TRUE);
		//$this->load->view ('siswa', $data);
		//$this->load->view('siswa');
	}
	function getdata_siswaAll()
	{
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('admin/data-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function tambah_siswa()
	{
		$this->Admin_model->tambah_siswa();
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['generate_nis']= $this->Admin_model->generate_nis();
		$data1['generate_nis']= $this->Admin_model->generate_nis();  
		$data1['getid_ortu']= $this->Admin_model->getid_ortu(); 
		$data1['getid_wali']= $this->Admin_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/data-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
}
