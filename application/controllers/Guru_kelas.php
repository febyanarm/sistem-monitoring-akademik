<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru_kelas extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		/*$this->load->library('session');
		$id_user = $this-
		if ($this->session->userdata('id_login')=="" OR $this->session->userdata('role')!="Guru Kelas"){
			$this->session->unset_userdata('id_login');
			//$this->session->unset_userdata('role');
			session_destroy();
			echo "<script>alert('".$this->session->userdata('role')."');</script>";
			//redirect('Auth/Index');
		}*/
		$this->load->database();
		$this->load->model('Siswa_model');
		$this->load->model('Guru_model');
		$this->load->model('Mapel_model');
		$this->load->model('Kelas_model');
		$this->load->model('Kbm_model');
		$this->load->model('Nilai_model');
		$this->load->model('Grafik_model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//================NAVIGASI ==============================/
	public function dashboard()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data1['report']= $this->Grafik_model->report(); 
		$data1['coba']= $this->Grafik_model->coba(); 
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('guru_kelas/grafik', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	public function loadData(){
			 $oadType=$this->input->post('loadType');
			 $oadId=$this->input->post('LoadId');
			 $data=$this->Nilai_model->jumlah_minggu($oadType,$oadId);
			 //$jumlah = $data->num_rows();
			 //echo "<script>alert('".$data."')</script>";
			 //echo $data;
			 //echo "<script>alert('".$jumlah."');</script>";
			 $HTML="";  
			    $HTML.="
			    			  <label class='col-md-4 col-sm-4 col-xs-12' for='no_induk' style='margin-top : 1.5%;'>
                                 PENILAIAN MINGGU KE - ".$data."
                              </label>
                        ";
			 echo $HTML;
			 }
			 public function loadDataPTS(){
			 $oadType=$this->input->post('loadType');
			 $oadId=$this->input->post('LoadId');
			 $data=$this->Nilai_model->jumlah_PTS($oadType,$oadId);
			 //$jumlah = $data->num_rows();
			 //echo "<script>alert('".$data."')</script>";
			 //echo $data;
			 //echo "<script>alert('".$jumlah."');</script>";
			 $HTML="";  
			    $HTML.="
			    			  <label class='col-md-5 col-sm-5 col-xs-12' for='no_induk' style='margin-top : 1.5%;'>
                                PENILAIAN TENGAH SEMESTER KE - ".$data."
                              </label>
                        ";
			 echo $HTML;
			 }
			 public function loadDataPAS(){
			 $oadType=$this->input->post('loadType');
			 $oadId=$this->input->post('LoadId');
			 $data=$this->Nilai_model->jumlah_PAS($oadType,$oadId);
			 //$jumlah = $data->num_rows();
			 //echo "<script>alert('".$data."')</script>";
			// $jumlah = count($data);
			// $total = $jumlah + 1;
			 //echo $data;
			 //echo "<script>alert('".$jumlah."');</script>";
			 $HTML="";  
			    $HTML.="
			    			  <label class='col-md-5 col-sm-5 col-xs-12' for='no_induk' style='margin-top : 1.5%;'>
                                PENILAIAN SEMESTER KE - ".$data."
                              </label>
                        ";
			 echo $HTML;
			 }
	public function index()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data['isi'] = $this->load->view('guru_kelas/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function daftar_siswa()
	{
	  	$data1['id_user'] = $this->session->userdata('id_user');
		$data1['role'] = $this->session->userdata('role');
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('guru_kelas/daftar-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function PH()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_siswaKelas']= $this->Siswa_model->getdata_siswaKelas(); 
		$data1['getdata_kbm']= $this->Nilai_model->getdata_kbm(); 
		$data1['mapel_denganSikap']= $this->Nilai_model->mapel_denganSikap();
		$data['isi'] = $this->load->view('guru_kelas/input-nilai-PH', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function PTS()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_siswaKelas']= $this->Siswa_model->getdata_siswaKelas(); 
		$data1['getdata_kbm']= $this->Nilai_model->getdata_kbm(); 
		$data1['mapel_denganSikap']= $this->Nilai_model->mapel_denganSikap();
		$data['isi'] = $this->load->view('guru_kelas/input-nilai-PTS', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function PAS()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_siswaKelas']= $this->Siswa_model->getdata_siswaKelas(); 
		$data1['getdata_kbm']= $this->Nilai_model->getdata_kbm(); 
		$data1['mapel_denganSikap']= $this->Nilai_model->mapel_denganSikap();
		$data['isi'] = $this->load->view('guru_kelas/input-nilai-PAS', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}

	function input_nilai()
	{
		$kriteria = $this->input->post('kriteria');
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Nilai_model->input_nilai(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_siswaKelas']= $this->Siswa_model->getdata_siswaKelas(); 
		$data1['getdata_kbm']= $this->Nilai_model->getdata_kbm(); 
		$data1['mapel_denganSikap']= $this->Nilai_model->mapel_denganSikap();
		if($kriteria == 'PH'){
			echo "<script>alert('Nilai Berhasil Disimpan.');
				window.location.href='".base_url()."index.php/guru_kelas/PH';
			  </script>";
		}else 
		if($kriteria == 'PTS'){
			echo "<script>alert('Nilai Berhasil Disimpan.');
				window.location.href='".base_url()."index.php/guru_kelas/PTS';
			  </script>";
		}else 
		if($kriteria == 'PAS'){
			echo "<script>alert('Nilai Berhasil Disimpan.');
				window.location.href='".base_url()."index.php/guru_kelas/PAS';
			  </script>";
		}
		$this->load->view ('skin', $data);
	}
	//==================guru==========================//
	function do_upload(){
		//$id = $this->session->userdata('email');
        //$this->load->model('Data_pemilik');

		$config['upload_path'] = './assets/images/guru';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->do_upload();//upload the file to the above mentioned path
        $this->Guru_model->tambah_guru($this->upload->data());// pass the uploaded information to the model}
   		}
	function input_guru(){
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data();
		$this->do_upload();
		$data1['get_dataGuruAll']= $this->Guru_model->get_dataGuruAll();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data['isi'] = $this->load->view('admin/daftar-guru', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	//==================guru==========================//



}
