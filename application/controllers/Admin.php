<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		/*$this->load->library('session');
		if ($this->session->userdata('id_user')=="" OR $this->session->userdata('role')!="Admin"){
			$this->session->unset_userdata('id_user');
			$this->session->unset_userdata('role');
			session_destroy();
			echo"Anda Belum Login";
			redirect('Auth/Index');
		}*/
		$this->load->database();
		$this->load->model('Siswa_model');
		$this->load->model('Guru_model');
		$this->load->model('Mapel_model');
		$this->load->model('Kelas_model');
		$this->load->model('Kbm_model');
		$this->load->model('Login_model');
		$this->load->model('KD_model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//================NAVIGASI ==============================/
	public function dashboard()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('admin/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	public function loadData(){
			 $oadType=$this->input->post('loadType');
			 $oadId=$this->input->post('LoadId');
			 $data=$this->KD_model->getKD_mapel($oadType,$oadId);
			 //$jumlah = $data->num_rows();
			 //echo $data;
			 $jumlah = count($data);
			 //echo $data;
			 //echo "<script>alert('".$jumlah."');</script>";
			 $HTML="";  
			 if($jumlah > 0){
			   foreach($data as $list){
			    $HTML.="
                            <tr style='cursor: pointer;'>
                              <td>".$list->nama_mapel."</td>
                              <td>".$list->tingkat_kelas."</td>
                              <td>".$list->no_kd."</td>
                              <td>".$list->keterangan."</td>
                              <td>
                              <form action='lihatIPK' method='post' style='float:left;'>
                              <input type='hidden' name='kodeKD' value='".$list->id_kd."'>
                                <button type='submit' data-kd='".$list->id_kd."' data-nama='".$list->keterangan."'class='btn btn-default'><b>Lihat IPK</b></button>
                              </form>
                                <button type='button' data-kd='".$list->id_kd."' data-nomor='".$list->no_kd."' data-nama='".$list->keterangan."' id='pilih' class='btn btn-info' href='#myModal' data-toggle='modal' data-target='#myModal' ><b>Tambah IPK</b></button>
                              </td>
                            </tr>";
			   }
			  }
			 echo $HTML;
			 }
			 public function loadDataKEL(){
			 $oadType=$this->input->post('loadType');
			 $oadId=$this->input->post('LoadId');
			 $data=$this->Kelas_model->ambilKelas($oadType,$oadId);
			 //$jumlah = $data->num_rows();
			 //echo $data;
			 $jumlah = count($data);
			 //echo $data;
			 //echo "<script>alert('".$jumlah."');</script>";
			 $HTML="";  
			 if($jumlah > 0){
			   foreach($data as $list){
			    $HTML.="
                            <div class='col-md-4 col-lg-4 col-sm-4 col-xs-12'>
                                <input type='checkbox' class='flat' name='id_kelas1[]' id='kelas' value='".$list->id_kelas."' /> Kelas ".$list->tingkat_kelas." ".$list->nama_kelas."
                            </div>
                        ";
			   }
			  }
			 echo $HTML;
			 }
	function allvar(){

		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
	}
	public function index()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data['isi'] = $this->load->view('admin/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function daftar_siswa()
	{
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/daftar-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function tambah_siswa(){
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/tambah-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function daftar_guru()
	{
		$data1['get_dataGuruAll']= $this->Guru_model->get_dataGuruAll();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data['isi'] = $this->load->view('admin/daftar-guru', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function tambah_guru()
	{
		$data1['get_dataGuruAll']= $this->Guru_model->get_dataGuruAll();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data['isi'] = $this->load->view('admin/tambah-guru', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function data_mapel()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data['isi'] = $this->load->view('admin/mata-pelajaran', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function daftar_kelas()
	{
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/daftar-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function daftar_kbm()
	{
		$data1['getdata_kbm']= $this->Kbm_model->getdata_kbm(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/daftar-kbm', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function kelola_pembagian_kelas(){
		$data1['get_kelasSatu']= $this->Kelas_model->get_kelasSatu(); 
		$data1['get_siswaBaru']= $this->Siswa_model->get_siswaBaru();
		$data1['get_siswaPindahan']= $this->Siswa_model->get_siswaPindahan(); 
		$data1['get_siswaTinggal']= $this->Siswa_model->get_siswaTinggal();  
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas();   
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/kelola-pembagian-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function kelola_kbm(){
		$data1['getid_login']= $this->Login_model->getid_login();
		$data1['getid_kbm']= $this->Kbm_model->getid_kbm();
		$data1['getdata_mapelGMP']= $this->Mapel_model->getdata_mapelGMP();
		$data1['get_dataGuruKosong']= $this->Guru_model->get_dataGuruKosong();
		$data1['get_calonGuruMP']= $this->Guru_model->get_calonGuruMP();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data1['get_kelasOutWakel']= $this->Kbm_model->get_kelasOutWakel();
		$data['isi'] = $this->load->view('admin/kelola-kbm', $data1, TRUE);
		$this->load->view ('skin', $data);
	}


	function data_KD()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_KD']= $this->KD_model->getdata_KD(); 
		$data1['getdata_allKD']= $this->KD_model->getdata_allKD(); 
		$data1['getid_KD']= $this->KD_model->getid_KD(); 
		$data['isi'] = $this->load->view('admin/kompetensi-dasar', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}

	function data_IPK()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_KD']= $this->KD_model->getdata_KD(); 
		$data1['getdata_allKD']= $this->KD_model->getdata_allKD(); 
		$data['isi'] = $this->load->view('admin/data_ipk', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function lihatIPK() {
		$this->KD_model->lihatIPK(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['lihatIPK']= $this->KD_model->lihatIPK(); 
		$data1['judulLihatIPK']= $this->KD_model->judulLihatIPK();
		$data1['getdata_allKD']= $this->KD_model->getdata_allKD(); 
		$data['isi'] = $this->load->view('admin/lihatIPK', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	//==================FUNGSIONAL==========================/
	function do_upload1(){
		//$id = $this->session->userdata('email');
        //$this->load->model('Data_pemilik');

		$config['upload_path'] = './assets/images/siswa';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->do_upload();//upload the file to the above mentioned path
        $this->Siswa_model->tambah_siswa($this->upload->data());// pass the uploaded information to the model}
   		}
	function input_siswa()
	{
		echo"<script>alert('".$this->input->post('provinsi')."');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data();
		$this->do_upload1();
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis();  
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/daftar-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	//==================siswa==========================//

	//==================guru==========================//
	function do_upload(){
		//$id = $this->session->userdata('email');
        //$this->load->model('Data_pemilik');

		$config['upload_path'] = './assets/images/guru';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->do_upload();//upload the file to the above mentioned path
        $this->Guru_model->tambah_guru($this->upload->data());// pass the uploaded information to the model}
   		}
	function input_guru(){
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data();
		$this->do_upload();
		$data1['get_dataGuruAll']= $this->Guru_model->get_dataGuruAll();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data['isi'] = $this->load->view('admin/daftar-guru', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	//==================guru==========================//



	function tambah_mapel()
	{
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Mapel_model->tambah_mapel(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel();  
		$data['isi'] = $this->load->view('admin/mata-pelajaran', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function edit_mapel()
	{
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Mapel_model->edit_mapel(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel();  
		$data['isi'] = $this->load->view('admin/mata-pelajaran', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function hapus_mapel()
	{
		echo"<script>alert('Mata Pelajaran Berhasil Dihapus!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Mapel_model->hapus_mapel(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel();  
		$data['isi'] = $this->load->view('admin/mata-pelajaran', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function input_kelas()
	{
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Kelas_model->tambah_kelas(); 
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas(); 
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();  
		$data['isi'] = $this->load->view('admin/daftar-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function tambah_kbm()
	{ 
		echo $this->Kbm_model->tambah_kbm(); 
		$data1['getid_login']= $this->Login_model->getid_login();
		$data1['getid_kbm']= $this->Kbm_model->getid_kbm();
		$data1['get_dataGuruKosong']= $this->Guru_model->get_dataGuruKosong();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data1['get_kelasOutWakel']= $this->Kbm_model->get_kelasOutWakel();
		$data['isi'] = $this->load->view('admin/kelola-kbm', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function tambah_kbmGMP()
	{ 
		echo $this->Kbm_model->tambah_kbmGMP(); 
		$data1['getid_login']= $this->Login_model->getid_login();
		$data1['getid_kbm']= $this->Kbm_model->getid_kbm();
		$data1['getdata_mapelGMP']= $this->Mapel_model->getdata_mapelGMP();
		$data1['get_dataGuruKosong']= $this->Guru_model->get_dataGuruKosong();
		$data1['get_calonGuruMP']= $this->Guru_model->get_calonGuruMP();
		$data1['get_dataGuru']= $this->Guru_model->get_dataGuru();
		$data1['get_kelasOutWakel']= $this->Kbm_model->get_kelasOutWakel();
		$data['isi'] = $this->load->view('admin/kelola-kbm', $data1, TRUE);
		$this->load->view ('skin', $data);
	}

	function kelola_siswa_baru()
	{
		//var_dump($datas);
		//echo $datas[0];
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Siswa_model->kelola_siswa_baru(); 
		$data1['get_kelasSatu']= $this->Kelas_model->get_kelasSatu(); 
		$data1['get_siswaBaru']= $this->Siswa_model->get_siswaBaru();
		$data1['get_siswaPindahan']= $this->Siswa_model->get_siswaPindahan(); 
		$data1['get_siswaTinggal']= $this->Siswa_model->get_siswaTinggal();  
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas();   
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/kelola-pembagian-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function kelola_siswa_pindahan()
	{
		//var_dump($datas);
		//echo $datas[0];
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Siswa_model->kelola_siswa_pindahan(); 
		$data1['get_kelasSatu']= $this->Kelas_model->get_kelasSatu(); 
		$data1['get_siswaBaru']= $this->Siswa_model->get_siswaBaru();
		$data1['get_siswaPindahan']= $this->Siswa_model->get_siswaPindahan(); 
		$data1['get_siswaTinggal']= $this->Siswa_model->get_siswaTinggal();  
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas();   
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/kelola-pembagian-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function kelola_siswa_tinggal_kelas()
	{
		//var_dump($datas);
		//echo $datas[0];
		echo"<script>alert('Perubahan Berhasil Disimpan!');</script>";
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->Siswa_model->kelola_siswa_tinggal_kelas(); 
		$data1['get_kelasSatu']= $this->Kelas_model->get_kelasSatu(); 
		$data1['get_siswaBaru']= $this->Siswa_model->get_siswaBaru();
		$data1['get_siswaPindahan']= $this->Siswa_model->get_siswaPindahan(); 
		$data1['get_siswaTinggal']= $this->Siswa_model->get_siswaTinggal();  
		$data1['getdata_kelas']= $this->Kelas_model->getdata_kelas();   
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data1['getid_kelas']= $this->Kelas_model->generate_idKelas();
		$data['isi'] = $this->load->view('admin/kelola-pembagian-kelas', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function tambah_KD()
	{
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->KD_model->tambah_KD(); 
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data1['getdata_KD']= $this->KD_model->getdata_KD(); 
		$data1['getdata_allKD']= $this->KD_model->getdata_allKD(); 
		$data1['getid_KD']= $this->KD_model->getid_KD();   
		$data['isi'] = $this->load->view('admin/kompetensi-dasar', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function tambah_IPK()
	{
		//$data['email'] = $this->session->userdata('email');
		//$this->Data_pemilik->update_data(); 
		$this->KD_model->tambah_IPK(); 
		echo "<script>alert('Data IPK berhasil disimpan.');
				window.location.href='".base_url()."index.php/admin/data_IPK';
			  </script>";
}
}
