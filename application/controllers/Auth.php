<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encrypt');
		$this->load->helper('security');
	}
	public function index() {
		$this->load->view('index');
	}

	public function cek_login() {
		$password = $this->input->post('password');
		///$pass =  do_hash($password, 'md5');
		$data = array('nama_user' => $this->input->post('nama_user', TRUE),
						'katasandi' => $password
			);
		$this->load->model('Model_user'); // load model_user
		$hasil = $this->Model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['id_login'] = $sess->id_login;
				$sess_data['id_user'] = $sess->id_user;
				$sess_data['nama_user'] = $sess->nama_user;
				$sess_data['role'] = $sess->role;
				$sess_data['status'] = $sess->status;
				$sess_data['last_login'] = $sess->last_login;
				$this->session->set_userdata($sess_data);
			}

		$id_user = $this->session->userdata('id_user');
		$last_log = $this->session->userdata('last_login');
		$status = $this->session->userdata('status');
		$role = $this->session->userdata('role');	
					
////////////////////////////////////////////////admin////////////////////////////////////////////////////////
			if ($role=='Admin') {
				if ($status=='aktif') {
					$query = $this->db->query('select nama_guru from guru where no_induk_pegawai = "'.$id_user.'"');
					$nama_guru = $query->result_array();
					$data = array();
						foreach ($nama_guru as $key => $value) {
							$data[0] = $value['nama_guru'];
							//$data[0] = $value->namaguru;
						}
					$this->db->set('tanggal',$last_log);
					$this->db->where('id_user',$id_user);
					$this->db->update('login');
					echo "<script>alert('Anda masuk sebagai ".$role.". Selamat datang, ".$data[0]."');
							window.location.href='".base_url()."index.php/admin/dashboard';
						  </script>";	
					//redirect('admin/dashboard');
				}
				else if ($status=='tidak aktif') {		
					echo "<script>alert('Akun ini telah non-aktif. Harap hubungi bagian kurikulum dan kesiswaan untuk kembali mengaktifkannya. Terima Kasih');history.go(-1);</script>";
				}
			}
///////////////////////////////////////////////end of admin///////////////////////////////////////////////////

////////////////////////////////////////////////guru kelas////////////////////////////////////////////////////////
			elseif ($role=='Guru Kelas') {
				if ($status=='aktif') {
					$query = $this->db->query('select nama_guru from guru where no_induk_pegawai = "'.$id_user.'"');
					$nama_guru = $query->result();
					$data = array();
						foreach ($nama_guru as $key => $value) {
							$data[0] = $value->nama_guru;
						}
					$this->db->set('tanggal',$last_log);
					$this->db->where('id_user',$id_user);
					$this->db->update('login');
					echo "<script>alert('Anda masuk sebagai ".$role.". Selamat datang, ".$data[0]."');
							window.location.href='".base_url()."index.php/guru_kelas/dashboard';
						  </script>";	
					//redirect('admin/dashboard');
				}
				else if ($status=='tidak aktif') {		
					echo "<script>alert('Akun ini telah non-aktif. Harap hubungi bagian kurikulum dan kesiswaan untuk kembali mengaktifkannya. Terima Kasih');history.go(-1);</script>";
				}
			}
///////////////////////////////////////////////end of guru kelas///////////////////////////////////////////////////

////////////////////////////////////////////////guru kelas/////////////////////////////////////////////////////////
			elseif ($role=='Guru Mata Pelajaran') {
				if ($status=='aktif') {
					$query = $this->db->query('select nama_guru from guru where no_induk_pegawai = "'.$id_user.'"');
					$nama_guru = $query->result();
					$data = array();
						foreach ($nama_guru as $key => $value) {
							$data[0] =$value->nama_guru;
						}
					$this->db->set('tanggal',$last_log);
					$this->db->where('id_user',$id_user);
					$this->db->update('login');
					echo "<script>alert('Anda masuk sebagai ".$role.". Selamat datang, ".$data[0]."');
							window.location.href='".base_url()."index.php/guru_mapel/dashboard';
						  </script>";	
					//redirect('admin/dashboard');
				}
				else if ($status=='tidak aktif') {		
					echo "<script>alert('Akun ini telah non-aktif. Harap hubungi bagian kurikulum dan kesiswaan untuk kembali mengaktifkannya. Terima Kasih');history.go(-1);</script>";
				}
			}
///////////////////////////////////////////////end of guru kelas///////////////////////////////////////////////////

////////////////////////////////////////////////guru kelas/////////////////////////////////////////////////////////
			elseif ($role=='Orangtua Siswa') {
				if ($status=='aktif') {
					$query = $this->db->query('select nama_ayah from orangtua_siswa where id_orangtua = "'.$id_user.'"');
					$nama_ortu = $query->result();
					$data = array();
						foreach ($nama_ortu as $key => $value) {
							$data[0] = $value['nama_ayah'];
						}
					$this->db->set('tanggal',$last_log);
					$this->db->where('id_user',$id_user);
					$this->db->update('login');
					echo "<script>alert('Anda masuk sebagai ".$role." siswa. Selamat datang, Bapak ".$data[0]."');
							window.location.href='".base_url()."index.php/orangtua_wali/dashboard';
						  </script>";	
					//redirect('admin/dashboard');
				}
				else if ($status=='tidak aktif') {		
					echo "<script>alert('Akun ini telah non-aktif. Harap hubungi bagian kurikulum dan kesiswaan untuk kembali mengaktifkannya. Terima Kasih');history.go(-1);</script>";
				}
			}
///////////////////////////////////////////////end of guru kelas///////////////////////////////////////////////////

////////////////////////////////////////////////guru kelas////////////////////////////////////////////////////////
			elseif ($role=='Wali Siswa') {
				if ($status=='aktif') {
					$query = $this->db->query('select nama_wali from wali_siswa where id_wali = "'.$id_user.'"');
					$nama_wali = $query->result();
					$data = array();
						foreach ($nama_wali as $key => $value) {
							$data[0] = $value['nama_wali'];
						}
					$this->db->set('tanggal',$last_log);
					$this->db->where('id_user',$id_user);
					$this->db->update('login');
					echo "<script>alert('Anda masuk sebagai ".$role." siswa. Selamat datang, Bapak/Ibu ".$data[0]."');
							window.location.href='".base_url()."index.php/orangtua_wali/dashboard';
						  </script>";	
					//redirect('admin/dashboard');
				}
				else if ($status=='tidak aktif') {		
					echo "<script>alert('Akun ini telah non-aktif. Harap hubungi bagian kurikulum dan kesiswaan untuk kembali mengaktifkannya. Terima Kasih');history.go(-1);</script>";
				}
			}
///////////////////////////////////////////////end of guru kelas///////////////////////////////////////////////////
			/*elseif (($this->session->userdata('role')=='penyewa') && ($this->session->userdata('status')=='aktif')) {
			redirect('Penyewa');
			}
			elseif ($this->session->userdata('status')=='tidak aktif') {
			echo "<script>alert('Mohon konfirmasi alamat email anda terlebih dahulu.');history.go(-1);</script>";
			}*/
		}
		else {
			echo "<script>alert('Login Gagal. Cek username dan password anda.');history.go(-1);</script>";
		}
	}

	public function logout() {
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role');
		session_destroy();
		redirect('auth/index');
	}

}

?>