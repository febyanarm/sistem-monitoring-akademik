<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		/*$this->load->library('session');
		if ($this->session->userdata('id_user')=="" OR $this->session->userdata('role')!="Admin"){
			$this->session->unset_userdata('id_user');
			$this->session->unset_userdata('role');
			session_destroy();
			echo"Anda Belum Login";
			redirect('Auth/Index');
		}*/
		$this->load->database();
		$this->load->model('Guru_model');
		$this->load->model('Siswa_model');
		$this->load->model('Mapel_model');
		$this->load->model('Nilai_model');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function admin()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('admin/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	public function guru_kelas()
	{
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('guru_kelas/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	public function guru_mapel()
	{
		$data1['get_minggu']= $this->Nilai_model->get_minggu(); 
		$data1['hitung_guru']= $this->Guru_model->hitung_guru(); 
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data['isi'] = $this->load->view('guru_mapel/dashboard', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function data_siswa()
	{
		$data1['getdata_siswaAll']= $this->Siswa_model->getdata_siswaAll(); 
		$data1['getdata_siswa']= $this->Siswa_model->getdata_siswa(); 
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/data-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function tambah_siswa(){
		$data['generate_nis']= $this->Siswa_model->generate_nis();
		$data1['generate_nis']= $this->Siswa_model->generate_nis(); 
		$data1['getid_ortu']= $this->Siswa_model->getid_ortu(); 
		$data1['getid_wali']= $this->Siswa_model->getid_wali(); 
		$data['isi'] = $this->load->view('admin/tambah-siswa', $data1, TRUE);
		$this->load->view ('skin', $data);
	}
	function data_guru()
	{

		$data['isi'] = $this->load->view('admin/data-guru', NULL, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
	function data_mapel()
	{
		$data1['getdata_mapel']= $this->Mapel_model->getdata_mapel(); 
		$data1['getid_mapel']= $this->Mapel_model->getid_mapel(); 
		$data['isi'] = $this->load->view('admin/mata-pelajaran', $data1, TRUE);
		$this->load->view ('skin', $data);
		//$this->load->view('siswa');
	}
}
