<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation'); // digunakan untuk proses validasi yg di input
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//

	 public function getid_login() {
	  $tahun = date("Y");
	  $kode = 'LOG';
	  $query = $this->db->query("SELECT MAX(id_login) as max_id FROM login"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,12,3);
	  $id_login = $max_id1 +1;
	  $maxid_login = $tahun.$kode.sprintf("%06s",$id_login);
	  return $maxid_login;
	 }
		//-------------------------------------------------------------siswa-----------------------------------/
}