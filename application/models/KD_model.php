<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KD_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function getdata_KD(){
		$query = $this->db->get('tb_kd');
		return $query->result();
	}
	function getdata_allKD(){
	  $query = $this->db->query("SELECT tb_kd.*, mata_pelajaran.* FROM tb_kd, mata_pelajaran where tb_kd.id_mapel = mata_pelajaran.id_mapel"); 
		return $query->result();
	}
	function judulLihatIPK(){
		$id_kd = $this->input->post('kodeKD');
	  $query = $this->db->query("SELECT mata_pelajaran.*, tb_kd.* FROM tb_kd, mata_pelajaran where tb_kd.id_mapel = mata_pelajaran.id_mapel and tb_kd.id_kd = '$id_kd'"); 
		return $query->result();
	}
	function lihatIPK(){
		$id_kd = $this->input->post('kodeKD');
	  $query = $this->db->query("SELECT tb_kd.*, mata_pelajaran.*, tb_ipk.* FROM tb_kd, mata_pelajaran, tb_ipk where tb_kd.id_mapel = mata_pelajaran.id_mapel and tb_kd.id_kd = tb_ipk.id_kd and tb_kd.id_kd = '$id_kd'"); 
		return $query->result();
	}
	 public function getid_KD() {
	  $tahun = date("Ymd");
	  $kode = 'KOD';
	  $query = $this->db->query("SELECT MAX(id_kd) as max_id FROM tb_kd"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,11,3);
	  $id_kd = $max_id1 +1;
	  $maxid_kd = $tahun.$kode.sprintf("%03s",$id_kd);
	  return $maxid_kd;
	 }
	function tambah_KD(){
		$id_kd = $this->input->post('id_kd');
		$id_mapel = $this->input->post('id_mapel');
		$tingkat = $this->input->post('tingkat');
		$no_kd = trim($this->input->post('no_kd'),"_");
		$keterangan = $this->input->post('keterangan');

	  $query1 = $this->db->query("SELECT author FROM mata_pelajaran where id_mapel = '$id_mapel'"); 
	  $hasilGMP = $query1->result();
	  $dataGMP = array();
	  foreach ($hasilGMP as $key) {
	  	$dataGMP[0] = $key->author;
	  }


	  $query = $this->db->query("SELECT * FROM tb_kd where id_mapel = '$id_mapel' and tingkat_kelas = $tingkat and no_kd = '$no_kd'"); 
	  $jumlah = $query->num_rows();
	  $kriteria = substr($no_kd, 0, 1);
		if ($dataGMP[0] == "Guru Kelas") {	
			  if($jumlah == 0){
			  	if($kriteria == '1'){
				echo"<script>alert('Mohon maaf, KI 1 dan 2 tidak tersedia untuk Mata pelajaran Guru Kelas. silahkan masukkan Mata pelajaran yang lainnya atau hapus data sebelumnya. Terimakasih.');</script>";
			  	}
			  	else if($kriteria == '2'){
				echo"<script>alert('Mohon maaf, KI 1 dan 2 tidak tersedia untuk Mata pelajaran Guru Kelas. silahkan masukkan Mata pelajaran yang lainnya atau hapus data sebelumnya. Terimakasih.');</script>";
			  	}
			  	else if($kriteria == '3'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'PENGETAHUAN',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
				echo"<script>alert('Data KD berhasil disimpan.');</script>";
			  	}
			  	else if($kriteria == '4'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'KETERAMPILAN',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
				echo"<script>alert('Data KD berhasil disimpan.');</script>";
			  	}
			  }else{
				echo"<script>alert('Mohon maaf, Nomor KD sudah terdaftar. silahkan masukkan Nomor KD yang lain atau hapus data sebelumnya. Terimakasih.');</script>";
			  }
		}else if($dataGMP[0] == "Guru Mata Pelajaran"){
			  if($jumlah == 0){
			  	if($kriteria == '1'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'SPIRITUAL',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
			  	}
			  	else if($kriteria == '2'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'SOSIAL',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
			  	}
			  	else if($kriteria == '3'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'PENGETAHUAN',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
			  	}
			  	else if($kriteria == '4'){
				  	$data = array(
				        'id_kd' =>$id_kd,
				        'id_mapel' =>$id_mapel ,
				        'tingkat_kelas' => $tingkat,
				        'no_kd' => $no_kd,
				        'kriteria' => 'KETERAMPILAN',
				        'keterangan' => $keterangan
						);
					$query1 = $this->db->insert('tb_kd', $data);
			  	}
				echo"<script>alert('Data KD berhasil disimpan.');</script>";
			  }else{
				echo"<script>alert('Mohon maaf, Nomor KD sudah terdaftar. silahkan masukkan Nomor KD yang lain atau hapus data sebelumnya. Terimakasih.');</script>";
			  }
		}else{
				echo"<script>alert('".$hasilGMP."');</script>";
		}
	}
	public function getKD_mapel($oadType,$oadId){
		$query = $this->db->query("SELECT tb_kd.*, mata_pelajaran.* FROM tb_kd, mata_pelajaran where tb_kd.id_mapel = '$oadId' and tb_kd.id_mapel = mata_pelajaran.id_mapel"); 
		return $query->result();
	}
	function tambah_IPK(){
	  $tahun = date("Ymd");
	  $kode = 'IPK';
	  $query = $this->db->query("SELECT MAX(id_ipk) as max_id FROM tb_ipk"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,11,3);
	  $id_ipk = $max_id1 +1;
	  $maxid_ipk = $tahun.$kode.sprintf("%03s",$id_ipk);

		$id_kd = $this->input->post('id_kd');
		$no_kd = $this->input->post('no_kd');
		$ket_ipk = $this->input->post('ket_ipk');
		$no_kd = trim($this->input->post('no_kd'),"_");
		$keterangan = $this->input->post('ket_ipk');

	  $query1 = $this->db->query("SELECT MAX(id_ipk) as maks FROM tb_ipk where id_kd = '$id_kd'"); 
	  $jumlah = $query1->row_array();
	  $maks_id = $jumlah['maks']; 
	  $panjangKD = strlen($no_kd);
	  $maks_id1 =(int) substr($maks_id,$panjangKD,3);
	  $maks_no = $maks_id1+1;
	  
	  $kode = $no_kd.".".$maks_no;
	  $data = array(
		        'id_ipk' =>$maxid_ipk,
		        'id_kd' =>$id_kd,
		        'no_ipk' =>$kode ,
		        'keterangan' => $keterangan
				);
			$query1 = $this->db->insert('tb_ipk', $data);
	}
}