<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation'); // digunakan untuk proses validasi yg di input
   $this->load->database(); // Load our cart model for our entire class
   $this->load->helper(array('url','form')); // Load our cart model for our entire class
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//

	function getdata_siswa(){
		$query = $this->db->get('siswa');
		return $query->result();
	}
	function getdata_siswaAll(){
		$query = $this->db->query("select * FROM siswa join orangtua_siswa on siswa.id_orangtua = orangtua_siswa.id_orangtua");
		return $query->result();
	}
	function get_siswaBaru(){
		$query = $this->db->query("select * FROM siswa join orangtua_siswa on siswa.id_orangtua = orangtua_siswa.id_orangtua where status_siswa = 'Siswa Baru' and id_kelas = '' ");
		return $query->result();
	}
	function get_siswaPindahan(){
		$query = $this->db->query("select * FROM siswa join orangtua_siswa on siswa.id_orangtua = orangtua_siswa.id_orangtua where status_siswa = 'Siswa Pindahan' and id_kelas = '' ");
		return $query->result();
	}
	function get_siswaTinggal(){
		$query = $this->db->query("select * FROM siswa join kelas on siswa.id_kelas = kelas.id_kelas where status_siswa = 'Siswa Pindahan' or status_siswa = 'Siswa Baru' and kelas.tingkat_kelas > 1 ");
		return $query->result();
	}
	function getdata_siswaKelas(){
		$id_user = $this->session->userdata('id_user');
		$query = $this->db->query(" select DISTINCT siswa.* , kelas.* FROM mata_pelajaran,siswa,guru,tb_belajarmengajar,kelas where guru.no_induk_pegawai = '$id_user' and guru.no_induk_pegawai = tb_belajarmengajar.no_induk_pegawai and kelas.id_kelas = tb_belajarmengajar.id_kelas and siswa.id_kelas = tb_belajarmengajar.id_kelas and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mata_pelajaran.author = 'Guru Kelas'
			");
		return $query->result();
	}
	function getdata_siswaMapel(){
		$id_user = $this->session->userdata('id_user');
		$query = $this->db->query(" select DISTINCT siswa.* , kelas.* FROM mata_pelajaran,siswa,guru,tb_belajarmengajar,kelas where guru.no_induk_pegawai = '$id_user' and guru.no_induk_pegawai = tb_belajarmengajar.no_induk_pegawai and kelas.id_kelas = tb_belajarmengajar.id_kelas and siswa.id_kelas = tb_belajarmengajar.id_kelas and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mata_pelajaran.author = 'Guru Mata Pelajaran'");
		return $query->result();
	}
	 public function generate_nis() {
	  $tahun = date("Y");
	  //$kode = 'NOT';
	  $query = $this->db->query("SELECT MAX(no_induk) as max_id FROM siswa"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,4,6);
	  $no_induk = $max_id1 +1;
	  $maxno_induk = $tahun.sprintf("%06s",$no_induk);
	  return $maxno_induk;
	 }
	 public function getid_ortu() {
	  $tahun = date("Ymd");
	  $kode = 'ORT';
	  $query = $this->db->query("SELECT MAX(id_orangtua) as max_id FROM orangtua_siswa"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,9,6);
	  $id_orangtua = $max_id1 +1;
	  $maxid_orangtua = $tahun.$kode.sprintf("%03s",$id_orangtua);
	  return $maxid_orangtua;
	 }
	 public function getid_wali() {
	  $tahun = date("Ymd");
	  $kode = 'WAL';
	  $query = $this->db->query("SELECT MAX(id_wali) as max_id FROM wali_siswa"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,9,6);
	  $id_wali = $max_id1 +1;
	  $maxid_wali = $tahun.$kode.sprintf("%03s",$id_wali);
	  return $maxid_wali;
	 }
	function tambah_siswa($imgdata){
		$nama_wali = $this->input->post('nama_wali');
		$imgdata = $imgdata['file_name'];//get the content of the image using its path
		if($nama_wali != ''){
		 	//simpan data trans
			$data = array(
	        'no_induk' => $this->input->post('no_induk'),
	        'nama_siswa' => $this->input->post('nama_siswa'),
	        'tempat_lahir' => $this->input->post('tempat_lahir'),
	        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
	        'agama' => $this->input->post('agama'),
	        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
	        'foto' => $imgdata,
	        'pendidikan_sebelum' => $this->input->post('pendidikan_sebelum'),
	        'kelainan' => $this->input->post('kelainan'),
	        'saat_terj_kelainan' => $this->input->post('saat_terj_kelainan'),
	        'sebab' => $this->input->post('sebab'),
	        'alamat_siswa' => $this->input->post('alamat_siswa'),
	        'provinsi' => $this->input->post('provinsi'),
	        'kabupaten_kota' => $this->input->post('kabupaten_kota'),
	        'kecamatan' => $this->input->post('kecamatan'),
	        'desa' => $this->input->post('desa'),
	        'id_orangtua' => $this->input->post('id_orangtua'),
	        'id_wali' => $this->input->post('id_wali')
			);
		$query = $this->db->insert('siswa', $data);
		//return $query->result();

		$data1 = array(
	        'id_orangtua' => $this->input->post('id_orangtua'),
	        'nama_ayah' => $this->input->post('nama_ayah'),
	        'nama_ibu' => $this->input->post('nama_ibu'),
	        'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
	        'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
	        'alamat_orangtua' => $this->input->post('alamat_orangtua'),
	        'provinsi' => $this->input->post('prov_ortu'),
	        'kabupaten_kota' => $this->input->post('kot_ortu'),
	        'kecamatan' => $this->input->post('kec_ortu'),
	        'desa' => $this->input->post('des_ortu')
			);
		$query1 = $this->db->insert('orangtua_siswa', $data1);

			$data2 = array(
	        'id_wali' => $this->input->post('id_wali'),
	        'nama_wali' => $this->input->post('nama_wali'),
	        'pekerjaan_wali' => $this->input->post('pekerjaan_wali'),
	        'alamat_wali' => $this->input->post('alamat_wali'),
	        'provinsi' => $this->input->post('prov_wali'),
	        'kabupaten_kota' => $this->input->post('kot_wali'),
	        'kecamatan' => $this->input->post('kec_wali'),
	        'desa' => $this->input->post('des_wali')
			);
		$query2 = $this->db->insert('wali_siswa', $data2);
	}else{
		$data = array(
	        'no_induk' => $this->input->post('no_induk'),
	        'nama_siswa' => $this->input->post('nama_siswa'),
	        'tempat_lahir' => $this->input->post('tempat_lahir'),
	        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
	        'agama' => $this->input->post('agama'),
	        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
	        'foto' => $imgdata,
	        'pendidikan_sebelum' => $this->input->post('pendidikan_sebelum'),
	        'kelainan' => $this->input->post('kelainan'),
	        'saat_terj_kelainan' => $this->input->post('saat_terj_kelainan'),
	        'sebab' => $this->input->post('sebab'),
	        'alamat_siswa' => $this->input->post('alamat_siswa'),
	        'provinsi' => $this->input->post('provinsi'),
	        'kabupaten_kota' => $this->input->post('kabupaten_kota'),
	        'kecamatan' => $this->input->post('kecamatan'),
	        'desa' => $this->input->post('desa'),
	        'id_orangtua' => $this->input->post('id_orangtua'),
	        'id_wali' => "-"
			);
		$query = $this->db->insert('siswa', $data);
		//return $query->result();

		$data1 = array(
	        'id_orangtua' => $this->input->post('id_orangtua'),
	        'nama_ayah' => $this->input->post('nama_ayah'),
	        'nama_ibu' => $this->input->post('nama_ibu'),
	        'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
	        'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
	        'alamat_orangtua' => $this->input->post('alamat_orangtua'),
	        'provinsi' => $this->input->post('prov_ortu'),
	        'kabupaten_kota' => $this->input->post('kabupaten_kota'),
	        'kecamatan' => $this->input->post('kecamatan'),
	        'desa' => $this->input->post('desa')
			);
		$query1 = $this->db->insert('orangtua_siswa', $data1);

	}
	
		//-------------------------------------------------------------siswa----------------------------------------------------------//
}
	function kelola_siswa_baru() {
		$datas = $this->input->post('nis');
		$kelas = $this->input->post('kelas');
		//$data = array();
			for ($i = 0; $i < count($datas); $i++)
	        {
					$query = $this->db->query("UPDATE siswa SET id_kelas = '$kelas' WHERE no_induk= '$datas[$i]' ");
					//var_dump($datas[$i]);
	        }
    }
    function kelola_siswa_pindahan() {
		$datas = $this->input->post('nis');
		$kelas1 = $this->input->post('kelas1');
		//$data = array();
			for ($i = 0; $i < count($datas); $i++)
	        {
					$query = $this->db->query("UPDATE siswa SET id_kelas = '$kelas1' WHERE no_induk= '$datas[$i]' ");
	        }
    }
    function kelola_siswa_tinggal_kelas() {
		$datas = $this->input->post('nis');
		$kelas = $this->input->post('kelas');
		//$data = array();
			for ($i = 0; $i < count($datas); $i++)
	        {
					$query = $this->db->query("UPDATE siswa SET id_kelas = '$kelas' WHERE no_induk= '$datas[$i]' ");
	        }
    }
}