<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function getdata_mapel(){
		$query = $this->db->get('mata_pelajaran');
		return $query->result();
	}
	function getdata_mapelGMP(){
		$this->db->where('author', 'Guru Mata Pelajaran');
		$query = $this->db->get('mata_pelajaran');
		return $query->result();
	}
	function jumlah_mapel(){
		$query = $this->db->get('mata_pelajaran');
		return $query->num_rows();
	}
	 public function getid_mapel() {
	  $tahun = date("Ymd");
	  $kode = 'PEL';
	  $query = $this->db->query("SELECT MAX(id_mapel) as max_id FROM mata_pelajaran"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,12,3);
	  $id_mapel = $max_id1 +1;
	  $maxid_mapel = $tahun.$kode.sprintf("%03s",$id_mapel);
	  return $maxid_mapel;
	 }
	function tambah_mapel(){
	 	$data = array(
	        'id_mapel' => $this->input->post('id_mapel'),
	        'author' => $this->input->post('author'),
	        'alias' => $this->input->post('alias'),
	        'nama_mapel' => $this->input->post('nama_mapel')
			);
		$query = $this->db->insert('mata_pelajaran', $data);
	}
	function edit_mapel(){
		$this->db->set('author',$this->input->post('author1'));
		$this->db->set('nama_mapel',$this->input->post('nama_mapel1'));
		$this->db->where('id_mapel', $this->input->post('id_mapel1'));
		$this->db->update('mata_pelajaran'); 
	
	}
	function hapus_mapel(){
		$this->db->where('id_mapel', $this->input->post('hapus_mapel'));
		$this->db->delete('mata_pelajaran');


		$this->db->where('id_mapel', $this->input->post('hapus_mapel'));
		$this->db->delete('tb_belajarmengajar');
	}
	public function get_idmapelKD($loadType,$loadId){
	  if($loadType=="mapelkd"){
	   $fieldList='id_jenis as id,jenis_mobil';
	   $table='jenis_mobil';
	   $fieldName='id_mobil';
	   $orderByField='id_mobil';      
	  } 
	  
	  $query = $this->db->query("SELECT MAX(id_mapel) as max_id FROM mata_pelajaran"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,12,3);
	  $id_mapel = $max_id1 +1;
	  $maxid_mapel = $tahun.$kode.sprintf("%03s",$id_mapel);
	  return $maxid_mapel;
	 }
}