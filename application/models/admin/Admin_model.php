<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function tambah_siswa(){
		 	//simpan data trans
			$data = array(
	        'no_induk' => $this->input->post('no_induk')
			);
		$query = $this->db->insert('transaksi', $data);
		return $query->result();
    }
}