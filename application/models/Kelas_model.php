<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//

	function getdata_kelas(){
		$query = $this->db->get('kelas');
		return $query->result();
	}
	function get_kelasSatu(){
		$query = $this->db->query("select * from kelas where tingkat_kelas = '1'");
		return $query->result();
	}
	function ambilKelas($oadType,$oadId){
		$query = $this->db->query("select DISTINCT kelas.* from kelas,tb_belajarmengajar where kelas.id_kelas NOT IN (select kelas.id_kelas from kelas,tb_belajarmengajar where kelas.id_kelas = tb_belajarmengajar.id_kelas and tb_belajarmengajar.id_mapel = '$oadId')");
		return $query->result();
	}
	 public function generate_idKelas() {
	  $tahun = date("Y");
	  $kode = 'KEL';
	  $query = $this->db->query("SELECT MAX(id_kelas) as max_id FROM kelas"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,12,3);
	  $id_kelas = $max_id1 +1;
	  $maxid_kelas = $tahun.$kode.sprintf("%06s",$id_kelas);
	  return $maxid_kelas;
	 }
	function tambah_kelas(){
			$data = array(
	        'id_kelas' => $this->input->post('id_kelas'),
	        'nama_kelas' => $this->input->post('nama_kelas'),
	        'tingkat_kelas' => $this->input->post('tingkat_kelas')
			);
		$query = $this->db->insert('kelas', $data);
	}
		//-------------------------------------------------------------siswa-----------------------------------/
}