<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation'); // digunakan untuk proses validasi yg di input
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//

	function getdata_kbm(){
	  $id_user = $this->session->userdata('id_user');
	  $query = $this->db->query("select tb_belajarmengajar.*,  mata_pelajaran.nama_mapel from mata_pelajaran,tb_belajarmengajar where mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and tb_belajarmengajar.no_induk_pegawai = '$id_user' and mata_pelajaran.author = 'Guru Kelas'"); 
		return $query->result();
	}
	function mapel_denganSikap(){
		$_1 = '1.';
		$_2 = '2.';
	  $query = $this->db->query("select mata_pelajaran.* from mata_pelajaran, tb_kd, tb_belajarmengajar where mata_pelajaran.id_mapel = tb_kd.id_mapel and mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and (tb_kd.no_kd like '$_1' or tb_kd.no_kd like '$_2')"); 
		return $query->result();
	}
	function ambil_mapelGMP($oadType,$oadId){
	  $id_user = $this->session->userdata('id_user');
	  $query = $this->db->query("select tb_belajarmengajar.*,  mata_pelajaran.nama_mapel, siswa.id_kelas, siswa.no_induk from mata_pelajaran,tb_belajarmengajar,siswa where tb_belajarmengajar.id_kelas = siswa.id_kelas and mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and tb_belajarmengajar.no_induk_pegawai = '$id_user' and siswa.no_induk = '$oadId' and mata_pelajaran.author = 'Guru Mata Pelajaran'"); 
		return $query->result();
	}
	function ambil_mapelGK($oadType,$oadId){
	  $id_user = $this->session->userdata('id_user');
	  $query = $this->db->query("select tb_belajarmengajar.*,  mata_pelajaran.nama_mapel, siswa.id_kelas, siswa.no_induk from mata_pelajaran,tb_belajarmengajar,siswa where tb_belajarmengajar.id_kelas = siswa.id_kelas and mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and tb_belajarmengajar.no_induk_pegawai = '$id_user' and siswa.no_induk = '$oadId' and mata_pelajaran.author = 'Guru Kelas'"); 
		return $query->result();
	}
	public function jumlah_minggu($oadType,$oadId){
	  $id_user = $this->session->userdata('id_user');
		$role = $this->session->userdata('role');
	  			if(date('m') < 7){
	  				$year = date('Y');
	  				$first = $year-1;
	  				$last = date('Y');
								}
					else{
	  				$year = date('Y');
	  				$first = date('Y');
	  				$last = $year+1;
				}

	  				$awal = $first."-07-01";
	  				$akhir = $last."-06-30";
		$query = $this->db->query("SELECT MAX(periode) as maks FROM nilai_th_berjalan,tb_belajarmengajar,mata_pelajaran where no_induk = '$oadId' and kriteria = 'PH' and nilai_th_berjalan.no_induk_pegawai = '$id_user' and tgl_input between '$awal' and '$akhir' and kriteria = 'PH' and tb_belajarmengajar.id_jadwal = nilai_th_berjalan.id_jadwal and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mata_pelajaran.author = '$role'"); 
			 //echo "<script>alert('".$query."')</script>";
					 $jumlah = $query->row_array();
					 $jum = $jumlah['maks'];
					 $periode = $jum+1;
					 return $periode;
	}
	public function jumlah_PTS($oadType,$oadId){
		$id_user = $this->session->userdata('id_user');
		$role = $this->session->userdata('role');
	  			if(date('m') < 7){
	  				$year = date('Y');
	  				$first = $year-1;
	  				$last = date('Y');
								}
					else{
	  				$year = date('Y');
	  				$first = date('Y');
	  				$last = $year+1;
				}

	  				$awal = $first."-07-01";
	  				$akhir = $last."-06-30";
		$query = $this->db->query("SELECT MAX(periode) as maks FROM nilai_th_berjalan,tb_belajarmengajar,mata_pelajaran where no_induk = '$oadId' and kriteria = 'PH' and nilai_th_berjalan.no_induk_pegawai = '$id_user' and tgl_input between '$awal' and '$akhir' and kriteria = 'PTS' and tb_belajarmengajar.id_jadwal = nilai_th_berjalan.id_jadwal and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mata_pelajaran.author = '$role'");
			 //echo "<script>alert('".$query."')</script>";
					 $jumlah = $query->row_array();
					 $jum = $jumlah['maks'];
					 $periode = $jum+1;
					 return $periode;
	}
	public function jumlah_PAS($oadType,$oadId){
		$id_user = $this->session->userdata('id_user');
		$role = $this->session->userdata('role');
	  			if(date('m') < 7){
	  				$year = date('Y');
	  				$first = $year-1;
	  				$last = date('Y');
								}
					else{
	  				$year = date('Y');
	  				$first = date('Y');
	  				$last = $year+1;
				}

	  				$awal = $first."-07-01";
	  				$akhir = $last."-06-30";
		$query = $this->db->query("SELECT MAX(periode) as maks FROM nilai_th_berjalan,tb_belajarmengajar,mata_pelajaran where no_induk = '$oadId' and kriteria = 'PH' and nilai_th_berjalan.no_induk_pegawai = '$id_user' and tgl_input between '$awal' and '$akhir' and kriteria = 'PAS' and tb_belajarmengajar.id_jadwal = nilai_th_berjalan.id_jadwal and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mata_pelajaran.author = '$role'");
			 //echo "<script>alert('".$query."')</script>";
					 $jumlah = $query->row_array();
					 $jum = $jumlah['maks'];
					 $periode = $jum+1;
					 return $periode;
	}
	public function is_KISS(){
		$id_user = $this->session->userdata('id_user');
		$query = $this->db->query("SELECT * FROM tb_kd,tb_belajarmengajar,mata_pelajaran where `mata_pelajaran`.`id_mapel` = tb_kd.id_mapel and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and tb_belajarmengajar.no_induk_pegawai='$id_user' and tb_kd.no_kd LIKE '1.%' or tb_kd.no_kd LIKE '2.%'");
			 //echo "<script>alert('".$query."')</script>";
		return $query->result();
	}
	function jumlah_smt(){
		$_1 = '1.';
		$_2 = '2.';
	  $query = $this->db->query("select mata_pelajaran.* from mata_pelajaran, tb_kd, tb_belajarmengajar where mata_pelajaran.id_mapel = tb_kd.id_mapel and mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and (tb_kd.no_kd like '$_1' or tb_kd.no_kd like '$_2')"); 
		return $query->result();
	}
	 public function getid_kbm() {
	  $tahun = date("Y");
	  $kode = 'JAD';
	  $query = $this->db->query("SELECT MAX(id_jadwal) as max_id FROM tb_belajarmengajar"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,9,6);
	  $id_jadwal = $max_id1 +1;
	  $maxid_jadwal = $tahun.$kode.sprintf("%06s",$id_jadwal);
	  return $maxid_jadwal;
	 }
	function input_nilai(){

		$kriteria = $this->input->post('kriteria');
		$peng = $this->input->post('peng');
		$ket = $this->input->post('ket');
		$id_jadwal = $this->input->post('id_jadwal');
		$no_induk = $this->input->post('nis');

		$role = $this->session->userdata('role');

		if(date('m') < 7){
	  				$year = date('Y');
	  				$first = $year-1;
	  				$last = date('Y');
								}
					else{
	  				$year = date('Y');
	  				$first = date('Y');
	  				$last = $year+1;
				}

	  				$awal = $first."-07-01";
	  				$akhir = $last."-06-30";

 		$tahun = date("Y");
		$kode = 'NIL';
		$query = $this->db->query("SELECT MAX(id_nilai) as max_id FROM nilai"); 
		$row = $query->row_array();
		$max_id = $row['max_id']; 
		$max_id1 =(int) substr($max_id,9,6);
		$id_nilai = $max_id1 +1;
		$maxid_nilai = $tahun.$kode.sprintf("%06s",$id_nilai);
		
		if($kriteria = "PH"){
					 $query1 = $this->db->query("SELECT MAX(periode) as max_id FROM nilai_th_berjalan,tb_belajarmengajar,mata_pelajaran where no_induk = '$no_induk' and tgl_input between '$awal' and '$akhir' and kriteria = 'PH' and tb_belajarmengajar.id_jadwal = nilai_th_berjalan.id_jadwal and tb_belajarmengajar.id_mapel = mata_pelajaran.id_mapel and mapel.author = '$role'");
					 $jumlah = $query1->row_array();
					 $jum = $jumlah['max_id'];
					 $periode = $jum+1;
			}else if($kriteria = "PTS"){
					 $query1 = $this->db->query("SELECT MAX(periode) as max_id FROM nilai_th_berjalan where no_induk = '$no_induk' and tgl_input between '$awal' and '$akhir' and kriteria = 'PTS'");
					 $jumlah = $query1->row_array();
					 $jum = $jumlah['max_id'];
					 $periode = $jum+1;
			}else if($kriteria = "PAS"){
					 $query1 = $this->db->query("SELECT MAX(periode) as max_id FROM nilai_th_berjalan where no_induk = '$no_induk' and tgl_input between '$awal' and '$akhir' and kriteria = 'PAS'");
					 $jumlah = $query1->row_array();
					 $jum = $jumlah['max_id'];
					 $periode = $jum+1;
			}
//var_dump($jum);
	 	$data = array(
        'id_nilai' => $maxid_nilai,
        'no_induk' => $no_induk,
        'kriteria' => $kriteria,
        'periode' => $periode
		);
		$this->db->insert('nilai', $data);

			for ($i = 0; $i < count($id_jadwal); $i++)
	        {
	        	  $tahun = date("Y");
				  $kode = 'DET';
				  $query = $this->db->query("SELECT MAX(id_detail) as max_id FROM detail_nilai"); 
				  $row = $query->row_array();
				  $max_id = $row['max_id']; 
				  $max_id1 =(int) substr($max_id,9,6);
				  $id_detail = $max_id1 +1;
				  $maxid_detail = $tahun.$kode.sprintf("%06s",$id_detail);

					$datas = array(
        			'id_nilai' => $maxid_nilai,
			        'id_detail' => $maxid_detail,
			        'id_jadwal' => $id_jadwal[$i],
        			'pengetahuan' => $peng[$i],
			        'keterampilan' => $ket[$i],
			        'spiritual' => '-',
			        'sosial' => '-'
					);
					$this->db->insert('detail_nilai', $datas);
					//var_dump($datas[$i]);
	        }

	}
	function get_minggu(){

		$query = $this->db->query("SELECT DISTINCT periode FROM nilai where nilai.kriteria = 'PH'"); 
			 //echo "<script>alert('".$query."')</script>";
		return $query->result();
		//-------------------------------------------------------------siswa-----------------------------------/
	}
	function rata_PH(){
	  $id_user = $this->session->userdata('id_user');
		$query = $this->db->query("SELECT detail_nilai.pengetahuan, detail_nilai.keterampilan, siswa.nama_siswa FROM detail_nilai,nilai,tb_belajarmengajar,siswa where nilai.kriteria = 'PH' and tb_belajarmengajar.no_induk_pegawai = '$id_user' AND tb_belajarmengajar.id_jadwal = detail_nilai.id_jadwal and nilai.id_nilai = detail_nilai.id_nilai AND nilai.no_induk = siswa.no_induk"); 
			 //echo "<script>alert('".$query."')</script>";
		return $query->result();
		
	}
}