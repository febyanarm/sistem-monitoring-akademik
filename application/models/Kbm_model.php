<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kbm_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation'); // digunakan untuk proses validasi yg di input
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//

	function getdata_kbm(){
	  $query = $this->db->query("select kelas.*, tb_belajarmengajar.*, mata_pelajaran.*, guru.* from guru, mata_pelajaran,kelas,tb_belajarmengajar where kelas.id_kelas = tb_belajarmengajar.id_kelas and mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and guru.no_induk_pegawai = tb_belajarmengajar.no_induk_pegawai"); 
		return $query->result();
	}
	function get_kelasOutWakel(){
	  $query = $this->db->query("select DISTINCT kelas.* from kelas where kelas.id_kelas NOT IN (select id_kelas from tb_belajarmengajar,mata_pelajaran where mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and mata_pelajaran.author = 'Guru Kelas')"); 
		return $query->result();
	}
	 public function getid_kbm() {
	  $tahun = date("Y");
	  $kode = 'JAD';
	  $query = $this->db->query("SELECT MAX(id_jadwal) as max_id FROM tb_belajarmengajar"); 
	  $row = $query->row_array();
	  $max_id = $row['max_id']; 
	  $max_id1 =(int) substr($max_id,12,3);
	  $id_jadwal = $max_id1 +1;
	  $maxid_jadwal = $tahun.$kode.sprintf("%06s",$id_jadwal);
	  return $maxid_jadwal;
	 }
	function tambah_kbm(){
		$query = $this->db->query('select id_mapel from mata_pelajaran where author = "Guru Kelas"');
		$jumlah = $query->num_rows();
		$id_mapel = $query->result();
		$nip = $this->input->post('nip');
		$namaguru = $this->input->post('namaguru');
		$id_login = $this->input->post('id_login');

			$data = array();
		  
	 foreach ($id_mapel as $row):
	 	 $mapel = $row->id_mapel;

	 	  $tahun = date("Y");
		  $kode = 'JAD';
		  $query1 = $this->db->query("SELECT MAX(id_jadwal) as max_id FROM tb_belajarmengajar"); 
		  $row = $query1->row_array();
		  $max_id = $row['max_id']; 
		  $max_id1 =(int) substr($max_id,9,6);
		  $id_jadwal = $max_id1 +1;
		  $maxid_jadwal = $tahun.$kode.sprintf("%06s",$id_jadwal);
		  
		  
	 	$data = array(
        'id_jadwal' => $maxid_jadwal,
        'id_kelas' => $this->input->post('id_kelas'),
        'no_induk_pegawai' => $nip,
        'id_mapel' => $mapel
		);
		$this->db->insert('tb_belajarmengajar', $data);
	 endforeach;


$user = trim($namaguru," ");
$cutuser = substr($user, 0, 7);
$username = $cutuser.'gk';
$password = '1234';

		$data = array(
			        'id_login' => $id_login,
			        'nama_user' => $username,
			        'katasandi' => $password,
			        'id_user' => $nip,
			        'role' =>'Guru Kelas',
			        'status' => 'aktif',
					);
		$this->db->insert('login', $data);

		echo"<script>alert('Perubahan Berhasil Disimpan! Berikut adalah data login untuk ".$namaguru." | Nama User : ".$username." dan Kata Sandi : ".$password."');</script>";
		//for ($a=0;$a<$jumlah;$a++){
			//foreach ($id_mapel as $key => $value) {
				//		$data[$a] = $value['id_mapel'];
							//$data[0] = $value->namaguru;
				//}
				//echo $data[$a];
		//echo $data[$a];
		//}
	}
	function tambah_kbmGMP(){
		$nip = $this->input->post('nip');
		$namaguru = $this->input->post('namaguru');
		$kelas = $this->input->post('id_kelas1');
		$mapel = $this->input->post('mapel');
		//var_dump($kelas);
		//$data = array();
			for ($i = 0; $i < count($kelas); $i++)
	        {
	        	  $tahun = date("Y");
				  $kode = 'JAD';
				  $query1 = $this->db->query("SELECT MAX(id_jadwal) as max_id FROM tb_belajarmengajar"); 
				  $row = $query1->row_array();
				  $max_id = $row['max_id']; 
				  $max_id1 =(int) substr($max_id,9,6);
				  $id_jadwal = $max_id1 +1;
				  $maxid_jadwal = $tahun.$kode.sprintf("%06s",$id_jadwal);
				  //echo"".$maxid_jadwal."";
	        	  //var_dump($kelas[$i]);
					//echo"<script>alert('".$kelas[$i]."')<script>";
					$this->db->query("INSERT INTO `tb_belajarmengajar` (`id_jadwal`, `id_kelas`, `no_induk_pegawai`, `id_mapel`) VALUES ('$maxid_jadwal', '$kelas[$i]', '$nip', '$mapel')"); 
					///var_dump($datas[$i]);
	        }


$id_login = $this->input->post('id_login1');   
$user = trim($namaguru," ");
$cutuser = substr($user, 0, 7);
$username = $cutuser.'gmp';
$password = '1234';
			$data = array(
			        'id_login' => $id_login,
			        'nama_user' => $username,
			        'katasandi' => $password,
			        'id_user' => $nip,
			        'role' =>'Guru Mata Pelajaran',
			        'status' => 'aktif',
					);
		$this->db->insert('login', $data);
		echo"<script>alert('Perubahan Berhasil Disimpan! Berikut adalah data login untuk ".$namaguru." | Nama User : ".$username." dan Kata Sandi : ".$password."');</script>";
		//for ($a=0;$a<$jumlah;$a++){
	//foreach ($id_mapel as $key => $value) {
		//		$data[$a] = $value['id_mapel'];
					//$data[0] = $value->namaguru;
		//}
		//echo $data[$a];
//echo $data[$a];
//}
	}
		//-------------------------------------------------------------siswa-----------------------------------/
}