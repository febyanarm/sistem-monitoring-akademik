<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	//-------------------------------------------------------------siswa----------------------------------------------------------//
	function hitung_guru(){
		$this->db->distinct('');
	    $this->db->SELECT('guru.*');
	    $this->db->FROM('guru');
		$this->db->WHERE('guru.no_induk_pegawai !=', $this->session->userdata('id_user'));
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	function get_dataGuruKosong(){
		$query_hitung = $this->db->query("select * from tb_belajarmengajar");
		$jumlah = $query_hitung->num_rows();
		if ($jumlah > 0) {
				$query = $this->db->query("select DISTINCT guru.* from guru,tb_belajarmengajar where guru.no_induk_pegawai NOT IN (select no_induk_pegawai from tb_belajarmengajar,mata_pelajaran where mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and mata_pelajaran.author = 'Guru Kelas')");
				return $query->result();
		}else{
				$query = $this->db->query("select * from guru");
				return $query->result();
		}
		}
		function get_calonGuruMP(){
		$query_hitung = $this->db->query("select * from tb_belajarmengajar");
		$jumlah = $query_hitung->num_rows();
		if ($jumlah > 0) {
				$query = $this->db->query("select DISTINCT guru.* from guru,tb_belajarmengajar where guru.no_induk_pegawai NOT IN (select no_induk_pegawai from tb_belajarmengajar,mata_pelajaran where mata_pelajaran.id_mapel = tb_belajarmengajar.id_mapel and mata_pelajaran.author = 'Guru Mata Pelajaran')");
				return $query->result();
		}else{
				$query = $this->db->query("select * from guru");
				return $query->result();
		}
		}
	function get_dataGuruAll(){
		$query = $this->db->query("select * FROM guru");
		return $query->result();
		}
	function get_dataGuru(){
			$this->db->where('no_induk_pegawai', $this->session->userdata('id_user'));
			$query = $this->db->get('guru');
			return $query->result();
		}
	function tambah_guru($imgdata){
		$imgdata = $imgdata['file_name'];//get the content of the image using its path
		$data = array(
	        'no_induk_pegawai' => $this->input->post('no_induk_pegawai'),
	        'nama_guru' => $this->input->post('nama_guru'),
	        'tempat_lahir' => $this->input->post('tempat_lahir'),
	        'tgl_lahir' => $this->input->post('tgl_lahir'),
	        'foto' => $imgdata,
	        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
	        'alamat' => $this->input->post('alamat'),
	        'provinsi' => $this->input->post('provinsi'),
	        'kabupaten' => $this->input->post('kabupaten_kota'),
	        'kecamatan' => $this->input->post('kecamatan'),
	        'desa' => $this->input->post('desa'),
	        'mulai_mengajar' => $this->input->post('mulai_mengajar'),
	        'riw_pend' => $this->input->post('riw_pend'),
	        'telp' => $this->input->post('telp'),
	        'status' => $this->input->post('status'),
	        'jabatan' => $this->input->post('jabatan'),
	        'ijazah' => $this->input->post('ijazah'),
	        'sk' => $this->input->post('sk')
			);
		$query = $this->db->insert('guru', $data);
	}
}